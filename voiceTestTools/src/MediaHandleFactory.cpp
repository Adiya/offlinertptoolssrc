//
//  MediaHandleFactory.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/15.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "MediaHandleFactory.hpp"

#include "InMediaFilehandle.hpp"

#include "FFmpegUtils.h"

MediaHandleFactory::MediaHandleFactory()
{
    
}

MediaHandleFactory::~MediaHandleFactory()
{
    
}

MediaHandleBase *MediaHandleFactory::createMediaHandle()
{
    InMediaFilehandle *inMediaHandle = new InMediaFilehandle();
    
    return inMediaHandle;
}

void MediaHandleFactory::releaseMediaHandle(MediaHandleBase *handle)
{
    
}
