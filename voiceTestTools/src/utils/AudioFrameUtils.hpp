//
//  AudioFrameUtils.hpp
//  MediaRecordDemo
//
//  Created by xqm on 2017/9/5.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef AudioFrameUtils_hpp
#define AudioFrameUtils_hpp

#include <stdio.h>
#include <stdint.h>

#include <vector>

//#include "AudioEffectBase.h"

namespace MMTOOLS {

/*
 * 音频解码数据处理工具类，用于处理音频加减速、重采样等工作
 */
    
class AudioFrameUtils {
public:
    AudioFrameUtils();
    ~AudioFrameUtils();
    
    /*
     * @func 初始化工具类
     * @param void
     * @return 0 为正常, 其他为异常
     */
    int init();

    /*
     * @func 下一帧数据采样数
     * @param inSamples 输入采样数
     * @return >0 为正常, 其他为异常
     */
    int getNextFrame(int inSamples);
    
    /*
     * @func 数据转换
     * @param inData 输入音频数据；inSamples 输入音频等采样数；
     * outData 输出音频采样数； outMaxSamples 输出音频最大数
     * @return > 0 转换的数据量（单位samples） <=0 未转换出数据
     */
    int transfer(uint8_t *inData, int inSamples, uint8_t *outData, int outMaxSamples);
    
    /*
     * @func 刷新数据，在最后不再转换数据的时候使用
     * @param outData 输出音频采样数； outMaxSamples 输出音频最大数
     * @return > 0 转换的数据量（单位samples） <=0 未转换出数据
     */
    int flush(uint8_t *outData, int outMaxSamples);
    
    /*
     * @func 对音频工具类空间的释放
     * @param void
     * @return 0为正确，其他为异常。
     */
    int release();
    
private:
    
//    std::vector<AudioEffectBase *>audioEffectList;
    uint8_t *audioEffectBuf;
};
    
}


#endif /* AudioFrameUtils_hpp */
