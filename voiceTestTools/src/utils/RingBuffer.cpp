//
//  RingBuffer.cpp
//  libpcap_app
//
//  Created by Adiya on 2018/10/25.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "RingBuffer.hpp"

#include <string.h>
#include <stdlib.h>

#define is_power_of_2(x) ((x) != 0 && (((x) & ((x) - 1)) == 0))
#if defined(typeof)
#define RC_MIN(x,y) ({         \
typeof(x) _x = (x); \
typeof(y) _y = (y);    \
(void) (&_x == &_y);\
_x < _y ? _x : _y; })

#define RC_MAX(x,y) ({         \
typeof(x) _x = (x);    \
typeof(y) _y = (y);    \
(void) (&_x == &_y);\
_x > _y ? _x : _y; })

#else
#define RC_MAX(a,b)            (((a) > (b)) ? (a) : (b))
#define RC_MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

struct RingBuffer *ring_init(unsigned char *buffer, unsigned int size,
                       pthread_mutex_t *lock)
{
    struct RingBuffer *fifo = NULL;
    
    if(!is_power_of_2(size)){
        printf("size is not power of 2\n");
        return fifo;
    }
    
    fifo = (struct RingBuffer *)malloc(sizeof(struct RingBuffer));
    if (!fifo){
        printf("fifo malloc error\n");
        return fifo;
    }
    
    fifo->buffer = buffer;
    fifo->size = size;
    fifo->in = fifo->out = 0;
    fifo->lock = lock;
    
    return fifo;
}

struct RingBuffer *ring_alloc(unsigned int size, pthread_mutex_t *lock)
{
    unsigned char *buffer = NULL;
    struct RingBuffer *ret = NULL;
    
    buffer = (unsigned char *)malloc(size);
    if (!buffer) {
        printf("buffer malloc error\n");
        return ret;
    }
    
    ret = ring_init(buffer, size, lock);
    
    return ret;
}

void ring_free(struct RingBuffer *fifo)
{
    free(fifo->buffer);
    free(fifo);
}

unsigned int __ring_put(struct RingBuffer *fifo,
                        unsigned char *buffer, unsigned int len)
{
    unsigned int l;
    
    len = RC_MIN(len, fifo->size - fifo->in + fifo->out);
    
    l = RC_MIN(len, fifo->size - (fifo->in & (fifo->size - 1)));
    memcpy(fifo->buffer + (fifo->in & (fifo->size - 1)), buffer, l);
    memcpy(fifo->buffer, buffer + l, len - l);
    
    fifo->in += len;
    
    return len;
}

unsigned int __ring_get(struct RingBuffer *fifo,
                        unsigned char *buffer, unsigned int len)
{
    unsigned int l;
    
    len = RC_MIN(len, fifo->in - fifo->out);
    
    l = RC_MIN(len, fifo->size - (fifo->out & (fifo->size - 1)));
    memcpy(buffer, fifo->buffer + (fifo->out & (fifo->size - 1)), l);
    memcpy(buffer + l, fifo->buffer, len - l);
    
    fifo->out += len;
    return len;
}

