//
//  FFmpegUtils.h
//  MediaRecordDemo
//
//  Created by xqm on 2017/9/13.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef FFmpegUtils_h
#define FFmpegUtils_h

extern "C"
{
#include "libavformat/avformat.h"
#include "libavutil/imgutils.h"
#include "libavutil/opt.h"
#include "libavutil/frame.h"
#include "libavutil/eval.h"
#include "libavutil/avstring.h"
#include "libswresample/swresample.h"
#include "libavutil/audio_fifo.h"
#include "libavutil/time.h"
#if ANDROID
#include "libavcodec/jni.h"
#endif
}

#ifndef makeErrorStr
static char errorStr[AV_ERROR_MAX_STRING_SIZE];
#define makeErrorStr(errorCode) av_make_error_string(errorStr, AV_ERROR_MAX_STRING_SIZE, errorCode)
#endif

// fixed point to double
#define CONV_FP(x) ((double) (x)) / (1 << 16)
// double to fixed point
#define CONV_DB(x) (int32_t) ((x) * (1 << 16))

/*
 * @func 获取显示的旋转角度
 * @arg matrix[9] 旋转矩阵
 * @return > 0 反转角度 其他为异常
 */
double getDisplayRotation(const int32_t matrix[9]);

/*
 * @func 获取旋转矩阵
 * @arg st 视频流
 * @return > 0 反转角度 其他为异常
 */
double getDisplayMatrix(AVStream *st);

/*
 * @func 获取流中的配置项
 * @arg opts 输入项
 * @arg codec_id 解码器ID
 * @arg s 文件句柄
 * @arg st 媒体流
 * @arg codec 编码器
 * @return ==NULL 异常  其他为返回配置结构项
 */
AVDictionary *filter_codec_opts(AVDictionary *opts, enum AVCodecID codec_id,
                                AVFormatContext *s, AVStream *st, AVCodec *codec);

#endif /* FFmpegUtils_h */
