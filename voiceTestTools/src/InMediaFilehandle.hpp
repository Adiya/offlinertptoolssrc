//
//  InMediaFilehandle.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/15.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef InMediaFilehandle_hpp
#define InMediaFilehandle_hpp

#include <stdio.h>
#include <stdlib.h>

#include "MediaHandleBase.hpp"

class MediaHandleContext;
class FFmpegDemuxer;
class MediaDecoder;

typedef struct AVFormatContext AVFormatContext;
typedef struct AVCodecContext AVCodecContext;
typedef struct AVFrame AVFrame;
typedef struct AVAudioFifo AVAudioFifo;

class InMediaFilehandle : public MediaHandleBase {
public:
    InMediaFilehandle();
    ~InMediaFilehandle();
    
    /*
     * @func 打开多媒体句柄.
     * @param filename 文件名
     * @return 0 为正常; < 0为异常
     */
    int open(const char *filename);
    
    /*
     * @func 重新打开多媒体句柄.
     * @param filename 文件名
     * @return 0 为正常; < 0为异常
     */
    int reOpen(const char *filename);
    
    /*
     * @func 关闭多媒体句柄.
     * @param void
     * @return 0 为正常; < 0为异常
     */
    int close(void);
    
    /*
     * @func 读取解码包数据
     * @param frame 多媒体数据包
     * @param steamIndex 多媒体标示
     * @return 0 为正常; < 0为异常
     */
    int read(MMTOOLS::FrameData *frame, int steamIndex);
    
private:
    // 解封装器
    FFmpegDemuxer *mDemuxer;
    
    // 编解码器控制器
    AVCodecContext *avctx;
    
    AVFrame *pFrame;
    
    AVFormatContext *ifmatCtx;
    
    // 数据缓存
    uint8_t *mediaData;
    // 数据缓存大小
    unsigned int mediaDataSize;
    
    // 音频缓存队列
    AVAudioFifo *audioFifo;
};

#endif /* InMediaFilehandle_hpp */
