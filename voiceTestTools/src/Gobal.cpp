//
//  Gobal.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/19.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "Gobal.hpp"

#include "FFmpegUtils.h"
#include "ortp/ortp.h"

static bool isInit = false;

Gobal::Gobal()
{
    
}

Gobal::~Gobal()
{
    
}

bool Gobal::init()
{
    if (isInit) {
        av_log(NULL, AV_LOG_INFO, "Already gobal init!\n");
        return true;
    }
    
    ortp_init();
    ortp_scheduler_init();
    av_register_all();
    avformat_network_init();
    
    isInit = true;
    
    return  true;
}

void Gobal::deinit()
{
    ortp_exit();
    
    ortp_global_stats_display();
}

