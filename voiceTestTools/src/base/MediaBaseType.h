//
//  MediaBaseType.h
//  readFrame
//
//  Created by xqm on 2018/3/27.
//  Copyright © 2018年 xqm. All rights reserved.
//

#ifndef MediaBaseType_h
#define MediaBaseType_h

#ifndef MAX_STREAM_NUM
#define MAX_STREAM_NUM 8
#else
#undef MAX_STREAM_NUM
#define MAX_STREAM_NUM 8
#endif

typedef enum MediaType_t {
    MT_MEDIA_TYPE_NONE      = 0,
    MT_MEDIA_TYPE_VIDEO     = 0x01,
    MT_MEDIA_TYPE_AUDIO     = 0x02,
    MT_MEDIA_TYPE_DATA      = 0x04,
    MT_MEDIA_TYPE_SUBTITLE  = 0x05,
    MT_MEDIA_TYPE_ALL       = 0x07,
} MediaType_t;

typedef struct MTRational_t {
    int num;
    int den;
} MTRational_t;

#endif /* MediaBaseType_h */
