//
//  ThreadIPCContext.hpp
//  MediaRecordDemo
//
//  Created by xqm on 2017/8/18.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef ThreadIPCContext_hpp
#define ThreadIPCContext_hpp

#include <pthread.h>
#include <unistd.h>

#include <stdint.h>
#include <stdio.h>

namespace MMTOOLS {
    
class ThreadIPCContext {
public:
    /*
     * @func 构造函数，
     * @param initNums 初始化信号的数量.
     * @return void
     */
    ThreadIPCContext(int initNums);
    ~ThreadIPCContext();

    /*
     * @func pv操作的V累加操作.
     * @return == 0 为正常; < 0为异常
     */
    int condV();
    
    /*
     * @func pv操作的v累减操作.
     * @return == 0 为正常; < 0为异常
     */
    int condP();
    
    /*
     * @func P操作，可等待超时.
     * @param waitTime 超时时间 单位us
     * @return 0 为正常; < 0为异常
     */
    int condP(int64_t waitTime);
    
    /*
     * @func 信号量重置
     * @param void
     * @return 0 为正常; < 0为异常
     */
    int reset();
    /*
     * @func 释放pv控制器内存数据
     * @return == 0 为正常; < 0为异常
     */
    int release();
    
private:
    pthread_mutex_t condMux;
    pthread_cond_t cond;
    int condCnt;
};
}
#endif /* ThreadIPCContext_hpp */
