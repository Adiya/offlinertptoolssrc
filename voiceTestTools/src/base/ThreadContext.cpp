//
//  ThreadContext.cpp
//  MediaRecordDemo
//
//  Created by xqm on 2017/8/18.
//  Copyright © 2017年 xqm. All rights reserved.
//

#include <stdlib.h>
#include <string.h>

#include "ThreadContext.hpp"
#include "MTError.h"

typedef enum {
    MT_THREAD_PRIORITY_LOW,
    MT_THREAD_PRIORITY_NORMAL,
    MT_THREAD_PRIORITY_HIGH
} MTThreadPriority;

typedef struct ThreadParam_t {
    pthread_t id;
    void *arg;
    func threadFun;
    callback threadcb;
    char threadName[128];
    enum ThreadStat_t stat;
    int retval;
} ThreadParam_t;

namespace MMTOOLS {
    
static void *MTRunThread(void *arg)
{
    ThreadParam_t *thread = (ThreadParam_t *)arg;

#if ANDROID || __linux__
    pthread_setname_np(pthread_self(), thread->threadName);
#else
    pthread_setname_np(thread->threadName);
#endif
    return thread->threadFun(thread->arg);
}
    
ThreadContext::ThreadContext()
{
    threadArg = NULL;
}

ThreadContext::~ThreadContext()
{
    if (threadArg) {
        free(threadArg);
        threadArg = NULL;
    }
}

int ThreadContext::setFunction(func function, void *arg, const char *threadName)
{
    threadArg = (ThreadParam_t *)malloc(sizeof(ThreadParam_t));
    if (threadArg == NULL) {
        return AV_MALLOC_ERR;
    }
    
    threadArg->threadFun = function;
    threadArg->arg = arg;
    threadArg->stat = THREAD_IDLE;
    strncpy(threadArg->threadName, threadName, sizeof(this->threadArg->threadName) - 1);
    
    return 0;
}

int ThreadContext::start()
{
    int ret;
    
    ret = pthread_create(&threadArg->id, NULL, MTRunThread, threadArg);
    if (ret < 0) {
        return ret;
    }
    
    threadArg->stat = THREAD_RUNNING;
    
    return 0;
}

int ThreadContext::abort()
{
    threadArg->stat = THREAD_FORCEQUIT;
    
    return 0;
}

int ThreadContext::getThreadState()
{
    if (threadArg == NULL) {
        return AV_NOT_INIT;
    }
    
    return threadArg->stat;
}

int ThreadContext::markOver()
{
    threadArg->stat = THREAD_DEAD;
    
    return 0;
}

int ThreadContext::join()
{
    if (threadArg == NULL) {
        return AV_NOT_INIT;
    }
    
    threadArg->stat = THREAD_STOPPING;
    return pthread_join(threadArg->id, NULL);
}
    
int ThreadContext::detach()
{
    if (threadArg == NULL) {
        return AV_NOT_INIT;
    }
    
    return pthread_detach(threadArg->id);
}

}
