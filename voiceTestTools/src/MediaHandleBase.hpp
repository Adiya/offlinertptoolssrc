//
//  InMediaHandle.hpp
//  readFrame
//
//  Created by xqm on 2018/3/15.
//  Copyright © 2018年 xqm. All rights reserved.
//

#ifndef MediaHandleBase_hpp
#define MediaHandleBase_hpp

#include <stdio.h>

#include "FrameData.hpp"

typedef struct MediaParam_t MediaParam_t;
typedef struct FFmpegDemuxer FFmpegDemuxer;

class MediaHandleBase {
public:
    MediaHandleBase() {};
    
    virtual ~MediaHandleBase() {};
    
    /*
     * @func 获取句柄介绍.
     * @param void
     * @return 该句柄的一些简要介绍
     */
    virtual const char *getDescripe(void) { return NULL; };
    
    /*
     * @func 打开多媒体句柄.
     * @param filename 文件名
     * @return 0 为正常; < 0为异常
     */
    virtual int open(const char *filename) = 0;
    
    
    /*
     * @func 重新打开多媒体句柄.
     * @param filename 文件名
     * @return 0 为正常; < 0为异常
     */
    virtual int reOpen(const char *filename) { return -1; };
    /*
     * @func 关闭多媒体句柄.
     * @param void
     * @return 0 为正常; < 0为异常
     */
    virtual int close(void) = 0;

    /*
     * @func 获取多媒体信息
     * @param mediaParam 多媒体信息结构体
     * @return 0 为正常; < 0为异常
     */
    virtual int getMediaInfo(MediaParam_t *mediaParam) { return -1; };
    
    /*
     * @func 读取媒体数据
     * @param frame 多媒体数据
     * @return 0 为正常; < 0为异常
     */
    virtual int read(MMTOOLS::FrameData *frame) { return -1; };
    
    /*
     * @func 读取解码包数据
     * @param data 数据包
     * @param timestamp 需要获取的数据时间戳
     * @return 0 为正常; < 0为异常
     */
    virtual int read(MMTOOLS::FrameData *frame, int64_t timestamp)  { return -1; };
    
    /*
     * @func 跳过该帧
     * @param streamIndex 流标示
     * @return 0 为正常; < 0为异常
     */
    virtual int next(int streamIndex)  { return -1; };
    
    /*
     * @func 读取解码包数据
     * @param frame 多媒体数据包
     * @param steamIndex 多媒体标示
     * @return 0 为正常; < 0为异常
     */
    virtual int read(MMTOOLS::FrameData *frame, int steamIndex) { return -1; };
    
    /*
     * @func 读取解码包数据
     * @param frame 多媒体数据包
     * @param steamIndex 多媒体标示
     * @param refTime 获取参考时间
     * @return 0 为正常; < 0为异常
     */
    virtual int read(MMTOOLS::FrameData *frame, int steamIndex, int64_t refTime) { return -1; };
    
    /*
     * @func 获取文件句柄
     * @return == NULL 为异常，其他为控制多媒体的句柄
     */
    virtual void *getHandle() { return NULL; };
    
    /*
     * @func 获取缓存中最新帧的pts
     * @return AV_STAT_ERR，状态错误，其他为该帧的pts
     */
    virtual int64_t getBufferFrameNewestPts(int streamIndex) { return -1; };
    
    /*
     * @func 获取缓存中下一帧的pts
     * @return AV_STAT_ERR，状态错误，其他为该帧的pts
     */
    virtual int64_t getBufferFrameNextPts(int streamIndex) {return -1;};
    
    /*
     * @func 暂停状态
     * @return void
     */
    virtual void pause() { };
    
    /*
     * @func 恢复可读取状态
     * @return void
     */
    virtual void resume() {};
    
private:
    // 解封装器
    FFmpegDemuxer *mDemuxer;
    
protected:
    
    bool isOpen;
};
    

#endif /* MediaHandleBase_hpp */
