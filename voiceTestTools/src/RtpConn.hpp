//
//  RtpConn.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/13.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef RtpConn_hpp
#define RtpConn_hpp

#include "ortp/ortp.h"

#include <stdio.h>

class RtpConn {
public:
    RtpConn();
    ~RtpConn();

    /**
     * 创建发送端代码
     * @param dstIp 远端ip地址
     * @param dstPort 远端端口
     * @return ==0 为正常; < 0为异常
     */
    int create(const char *dstIp, int dstPort, int payload);

    /**
     * 配置数据类型
     * @param mediaType 媒体类型
     * @param mediaFormat 媒体格式
     * @return ==0 为正常; < 0为异常
     */
    int setDataFormat(int mediaType, int mediaFormat);
    
    /**
     * 创建发送端代码
     * @param data 发送数据
     * @param dataLen 数据量大小
     * @return ==0 为正常; < 0为异常
     */
    int sendData(uint8_t *data, size_t dataLen);
    
    /**
     * 释放rtp句柄等资源
     * @return ==0 为正常; < 0为异常
     */
    int release();
    
    /** 添加到集合
     *
     * @return ==0 为正常; < 0为异常
     */
    int addToSet(SessionSet *set);

    bool isSet();
    
private:
    
    SessionSet  *m_set;
    RtpSession  *m_pSession;     /** rtp会话句柄 */
    uint32_t        m_curTimeStamp; /** 当前时间戳 */
    int         m_timeStampInc; /** 时间戳增量 */
    uint32_t    m_ssrc;         /** 数据源标示 */
    int         m_payload;        /** 负载类型*/
//    char        *m_ssrc;        /** 数据源标示 */
    
};


#endif /* RtpConn_hpp */
