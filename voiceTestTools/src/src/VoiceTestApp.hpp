//
//  VoiceTestApp.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/13.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef VoiceTestApp_hpp
#define VoiceTestApp_hpp

#include <stdio.h>
#include <string>
#include <ortp/ortp.h>

typedef struct RingBuffer RingBuffer;


class RtpConn;
class MediaHandleBase;
namespace MMTOOLS {
    class FrameData;
}


class VoiceTestApp {
public:
    VoiceTestApp();
    ~VoiceTestApp();
    
    /**配制音频文件
     *
     * @param voiceFile 对应音频文件
     * @return ==0 为正常; < 0为异常
     */
    int setVoiceFile(const char *voiceFile);
    
    /**
     * @func 配制远端IP
     * @param ipAddr 远端ip地址
     * @return ==0 为正常; < 0为异常
     */
    int setRemoteIp(const char *ipAddr, int port);

    /**
     * @func 配制循环次数
     * @param loopTimes 循环次数
     * @return ==0 为正常; < 0为异常
     */
    int setLoop(int loopTimes);
    
    /** 启动发送rtp数据流程序
     *
     * @return ==0 为正常; < 0为异常
     */
    int process();
    
    /** 停止发送rtp数据流程序，可中途停止
     *
     * @return ==0 为正常; < 0为异常
     */
    int stop();
    
    /** 配置音频payload
     *
     * @return ==0 为正常; < 0为异常
     */
    int setAudioPayload(int payload);

    /** 加入放送集合
     *
     * @return ==0 为正常; < 0为异常
     */
    int addToSet(SessionSet *set);
    
    /** 准备动作
     *
     * @return ==0 为正常; < 0为异常
     */
    int prepare();
    
    /** 读取音频数据
     *
     * @return ==0 为正常; < 0为异常
     */
    int readAudioData();
    
    /** 发送音频数据
     *
     * @return ==0 为正常; < 0为异常
     */
    int sendAudioData();
    
    
    bool sessionSetIsSet();
    
private:
    
    std::string m_voiceFile;
    std::string m_ipAddr;
    int m_rtpPort;
    int m_payload;
    int m_time;
    bool m_stat;
    
private:
    MediaHandleBase *m_fileHandle;
    RtpConn *m_rtpConn;
    struct RingBuffer *m_ringbuffer;
    pthread_mutex_t mux;
    MMTOOLS::FrameData *frameData;

};

#endif /* VoiceTestApp_hpp */
