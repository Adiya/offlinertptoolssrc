//
//  InMediaFilehandle.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/15.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "InMediaFilehandle.hpp"
#include "FFmpegUtils.h"
#include "MTError.h"

#include "FrameData.hpp"

#ifndef MAX_STREAM_NUM
#define MAX_STREAM_NUM 8
#else
#undef MAX_STREAM_NUM
#define MAX_STREAM_NUM 8
#endif

InMediaFilehandle::InMediaFilehandle()
{
    ifmatCtx    = NULL;
    avctx       = NULL;
    isOpen      = false;
    pFrame      = NULL;
    mediaData   = NULL;
    mediaDataSize = 0;
}

InMediaFilehandle::~InMediaFilehandle()
{
    
}

int InMediaFilehandle::open(const char *filename)
{
    int ret = 0;
    AVCodec *pCodec;
    
    ifmatCtx = avformat_alloc_context();
    if (ifmatCtx == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Could not allocate context.\n");
        ret = ENOMEM;
        goto TAR_OUT;
    }
    
    ret = avformat_open_input(&ifmatCtx, filename, NULL, NULL);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Open media %s error! error reason %s\n", filename, makeErrorStr(ret));
        goto TAR_OUT;
    }
    
    ret = avformat_find_stream_info(ifmatCtx, NULL);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find media stream info\n");
        goto TAR_OUT;
    }
    
    if (ifmatCtx->pb) {
        ifmatCtx->pb->eof_reached = 0;
    }
    
    for (int i=0; i<ifmatCtx->nb_streams; i++) {
        AVStream *stream = ifmatCtx->streams[i];
        AVCodecParameters *codecParam = stream->codecpar;
        
        if (codecParam->codec_type == AVMEDIA_TYPE_VIDEO) {
            for (int j=0; j<stream->nb_index_entries; j++) {
                AVIndexEntry entry = stream->index_entries[j];
                av_log(NULL, AV_LOG_INFO, "cnt %d, flags %d min_distance %d pos %lld size %d timestamp %lld\n", j+1, entry.flags, entry.min_distance, \
                       entry.pos, entry.size, entry.timestamp);
            }
        } else if (codecParam->codec_type == AVMEDIA_TYPE_AUDIO) {
            pCodec = avcodec_find_decoder(codecParam->codec_id);
            if (pCodec == NULL) {
                av_log(NULL, AV_LOG_ERROR, "Cannot find decoder\n");
                ret = AVERROR_OPTION_NOT_FOUND;
                goto TAR_OUT;
            }
            avctx = avcodec_alloc_context3(pCodec);
            if (avctx == NULL) {
                av_log(NULL, AV_LOG_ERROR, "Alloc decoder context error!\n");
                ret = ENOMEM;
                goto TAR_OUT;
            }
            
            ret = avcodec_parameters_to_context(avctx, codecParam);
            if (ret < 0) {
                av_log(NULL, AV_LOG_ERROR, "Copy codec parameter error!\n");
                goto TAR_OUT;
            }
            ret = avcodec_open2(avctx, pCodec, NULL);
            if (ret < 0) {
                av_log(NULL, AV_LOG_ERROR, "Open decoder context error!\n");
                goto TAR_OUT;
            }
            audioFifo = av_audio_fifo_alloc(avctx->sample_fmt, avctx->channels, 1);
            if (audioFifo == NULL) {
                ret = ENOMEM;
                av_log(NULL, AV_LOG_ERROR, "Create audio fifo error!(sample format=%s channels=%d)\n", av_get_sample_fmt_name(avctx->sample_fmt), avctx->channels);
                goto TAR_OUT;
            }
        }
    }
    
    if (pFrame == NULL) {
        pFrame = av_frame_alloc();
        if (pFrame == NULL) {
            ret = ENOMEM;
            goto TAR_OUT;
        }
    }
    
    isOpen = true;
    
TAR_OUT:
    
    return ret;
}


int InMediaFilehandle::reOpen(const char *filename)
{
    return 0;
}

int InMediaFilehandle::read(MMTOOLS::FrameData *framedata, int steamIndex)
{
    int ret = 0;
    AVPacket pkt1, *pkt = &pkt1;
    uint8_t *audioData[MAX_STREAM_NUM] = {NULL};
    int lineSize[MAX_STREAM_NUM];
    int dataSize = 0;
    
    if (isOpen == false) {
        return ECANCELED;
    }
    
    while (true) {
        ret = av_read_frame(ifmatCtx, pkt);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Read packet fail[%s]\n", makeErrorStr(ret));
            break;
        }
        
        if (pkt->stream_index != steamIndex) {
            continue;
        }
        
        ret = avcodec_send_packet(avctx, pkt);
        av_packet_unref(pkt);
        if (ret == AVERROR(EAGAIN)) {
            av_log(NULL, AV_LOG_DEBUG, "Need more packet!\n");
            continue;
        }
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Send packet to decode context error![%s]\n", makeErrorStr(ret));
            break;
        }
        ret = avcodec_receive_frame(avctx, pFrame);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Decode packet error!\n");
            break;
        }
        
        dataSize = av_samples_get_buffer_size(NULL, avctx->channels, pFrame->nb_samples, (enum AVSampleFormat)avctx->sample_fmt, 1);
        if (dataSize > mediaDataSize) {
            av_fast_malloc((void *)&mediaData, &mediaDataSize, (size_t)dataSize);
            if (mediaData == NULL) {
                av_log(NULL, AV_LOG_ERROR, "Alloc media data error!\n");
                ret = ENOMEM;
                break;
            }
        }
        av_samples_fill_arrays(audioData, lineSize, mediaData, avctx->channels, pFrame->nb_samples, avctx->sample_fmt, 1);
        
        ret = av_audio_fifo_write(audioFifo, (void **)pFrame->data, pFrame->nb_samples);

        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Write audio data to fifo error!\n");
            break;
        }

        ret = av_audio_fifo_read(audioFifo, (void **)audioData, pFrame->nb_samples);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Read audio data to fifo error!\n");
            break;
        }

        framedata->setInMediaDataFormat(MT_MEDIA_TYPE_AUDIO, avctx->sample_fmt, steamIndex);
        framedata->inAudioFormat->sampleRate = avctx->sample_rate;
        framedata->inAudioFormat->sampleSize = avctx->frame_size;
        framedata->write(mediaData, dataSize);
        av_frame_unref(pFrame);
        
        break;
    }
    
    return ret;
}


int InMediaFilehandle::close(void)
{
    int ret = 0;
    
    isOpen = false;
    
    if (avctx) {
        avcodec_close(avctx);
        avcodec_free_context(&avctx);
    }
    
    if (ifmatCtx) {
        avformat_close_input(&ifmatCtx);
    }
    
    if (pFrame) {
        av_frame_free(&pFrame);
    }
    
    return ret;
}
