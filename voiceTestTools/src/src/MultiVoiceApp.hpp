//
//  multiVoiceApp.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/19.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef multiVoiceApp_hpp
#define multiVoiceApp_hpp

#include <stdio.h>
#include "RtpPayload.h"
#include <vector>

#include "VoiceTestApp.hpp"


class MultiVoiceApp {
public:
    MultiVoiceApp();
    ~MultiVoiceApp();
    
    int scandir(std::string path, int maxLimitFiles);
    
    int setServer(const char *ipAddr);
    
    int setAuidoFormat(int payload);
    
    int setRtpPortRange(int minPort, int maxPort);
    
    int setSSRC(int ssrc);
    
    int prepare();
    
    int process();
    
    int stop();
    
private:
    
    std::vector<std::string> fileList;
    std::vector<VoiceTestApp *> voiceTestList;
    std::string m_ipAddr;
    int m_ssrc;
    int m_audioPayload;
    int m_minRtpPort, m_maxRtpPort;
    int m_limit;
    SessionSet *m_set;
    bool m_stat;
};

#endif /* multiVoiceApp_hpp */
