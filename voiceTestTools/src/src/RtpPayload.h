//
//  RtpPayload.h
//  voice_test_tools
//
//  Created by Adiya on 2018/11/19.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef RtpPayload_h
#define RtpPayload_h

enum {
    PCMU_PAYLOAD    = 0,
    G729_PAYLOAD    = 18,
};

#endif /* RtpPayload_h */
