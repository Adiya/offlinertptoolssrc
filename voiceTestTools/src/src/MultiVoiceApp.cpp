//
//  MultiVoiceApp.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/19.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include <unistd.h>
#include <dirent.h>
#include <string>

#include <vector>
#include <iostream>
#include <sstream>

#include "MultiVoiceApp.hpp"
#include "VoiceTestApp.hpp"
#include "FFmpegUtils.h"

int getAllFiles(std::string path, std::vector<std::string>& files, int maxLimit)
{
    DIR * dp;
    struct dirent *filename;
    std::string audioPath;
    int fileCnt = 0;
    
    dp = opendir(path.c_str());
    if (!dp) {
        fprintf(stderr,"open directory error\n");
        return -1;
    }
    
    while ((filename = readdir(dp)) != NULL) {
        if (filename->d_name[0] == '.' || filename->d_type == DT_DIR) {
            continue;
        }
        
        std::string audioName(filename->d_name);
        if (audioName.substr(audioName.size() - 4) != ".wav") {
            continue;
        }
        printf("filename:%-10s\td_info:%llu\t d_reclen:%us\n",
               filename->d_name, filename->d_ino, filename->d_reclen);
        audioPath = path + '/' + filename->d_name;
        
        fileCnt++;
        if (fileCnt > maxLimit) {
            break;
        }
        
        files.push_back(audioPath);
    }
    
    closedir(dp);
    
    return fileCnt;
}

MultiVoiceApp::MultiVoiceApp()
{
    m_limit = 0;
    m_ssrc = 0;
    m_maxRtpPort = 0;
    m_maxRtpPort = 0;
    m_stat = false;
}

MultiVoiceApp::~MultiVoiceApp()
{
    
}

int MultiVoiceApp::scandir(std::string path, int maxLimitFiles)
{
    int ret = 0;
    VoiceTestApp *testApp;
    std::string audioFilename;
    
    ret = getAllFiles(path, fileList, maxLimitFiles);

    for(std::vector<std::string>::iterator it = fileList.begin(); it != fileList.end(); ++it) {
        testApp = new VoiceTestApp();
        if (testApp == NULL) {
            av_log(NULL, AV_LOG_ERROR, "Create audio test app error");
            return AVERROR(ENOMEM);
        }
        testApp->setLoop(1);
        audioFilename = *it;
        testApp->setVoiceFile(audioFilename.c_str());
        voiceTestList.push_back(testApp);
    }
    
    m_limit = maxLimitFiles;
    
    return ret;
}

int MultiVoiceApp::setServer(const char *ip)
{
    m_ipAddr = ip;
    
    return 0;
}

int MultiVoiceApp::setAuidoFormat(int payload)
{
    if (payload < 0) {
        return -1;
    }
    m_audioPayload = payload;
    
    return 0;
}

int MultiVoiceApp::setRtpPortRange(int minPort, int maxPort)
{
    m_minRtpPort = minPort;
    m_maxRtpPort = maxPort;
    
    return 0;
}

int MultiVoiceApp::setSSRC(int ssrc)
{
    m_ssrc = ssrc;
    
    return 0;
}

int MultiVoiceApp::prepare()
{
    VoiceTestApp *app;
    int ret;
    int rtpPort = m_minRtpPort;
    
    for (std::vector<VoiceTestApp *>::iterator it = voiceTestList.begin(); it != voiceTestList.end(); it++) {
        app = *it;
        app->setAudioPayload(m_audioPayload);
        av_log(NULL, AV_LOG_INFO, "Set server %s:%d\n", m_ipAddr.c_str(), rtpPort);
        app->setRemoteIp(m_ipAddr.c_str(), rtpPort);
        rtpPort += 2;
        if (rtpPort > m_maxRtpPort) {
            return AVERROR(EINVAL);
        }
        ret = app->prepare();
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "prepare audio test error!\n");
            return ret;
        }
    }
    m_set = session_set_new();
    
    return 0;
}

int MultiVoiceApp::process()
{
    VoiceTestApp *app;
    int ret = 0;
    m_stat = true;
    
    while (m_stat) {
        for (std::vector<VoiceTestApp *>::iterator it = voiceTestList.begin(); it != voiceTestList.end(); it++) {
            app = *it;
            ret = app->readAudioData();
            if (ret < 0) {
                av_log(NULL, AV_LOG_WARNING, "read Audio data error!\n");
                goto TAR_OUT;
            }
            
            app->addToSet(m_set);
            
            session_set_select(NULL, m_set, NULL);
            
            if (app->sessionSetIsSet()) {
                do {
                    ret = app->sendAudioData();
                    if (ret == AVERROR(EAGAIN)) {
                        break;
                    }
                
                    if (ret < 0) {
                        av_log(NULL, AV_LOG_ERROR, "Send audio data error!\n");
                        goto TAR_OUT;
                    }
                    
                } while (1);
            } else {
                av_log(NULL, AV_LOG_INFO, "Not in set\n");
//                break;
            }
        }
    }
    
    
TAR_OUT:
    
    return ret;
}

int MultiVoiceApp::stop()
{
    VoiceTestApp *app;
    int ret = 0;
    
    m_stat = false;
    for (std::vector<VoiceTestApp *>::iterator it = voiceTestList.begin(); it != voiceTestList.end(); it++) {
        app = *it;
    
        if (app == NULL) {
            continue;
        }
        
        app->stop();
    }
    
    
    return ret;
}
