//
//  Gobal.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/19.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef Gobal_hpp
#define Gobal_hpp

#include <stdio.h>

class Gobal {
public:
    Gobal();
    ~Gobal();
    
    static bool init();
    
    static void deinit();
    
private:
    
};

#endif /* Gobal_hpp */
