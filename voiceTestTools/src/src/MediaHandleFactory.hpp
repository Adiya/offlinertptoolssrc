//
//  MediaHandleFactory.hpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/15.
//  Copyright © 2018 xqm . All rights reserved.
//

#ifndef MediaHandleFactory_hpp
#define MediaHandleFactory_hpp

#include <stdio.h>

#include "MediaHandleBase.hpp"

class MediaHandleFactory {
public:
    MediaHandleFactory();
    ~MediaHandleFactory();
    
    
    static MediaHandleBase *createMediaHandle();
    
    static void releaseMediaHandle(MediaHandleBase *handle);
    
private:
    
};

#endif /* MediaHandleFactory_hpp */
