
if(HAVE_RT)
	list(APPEND LIBS rt)
endif()

if(LIBM)
	list(APPEND LIBS ${LIBM})
endif()

# General source files
file(GLOB SOURCE_FILES
    *.cpp
    base/*.cpp
    base/ipc/*.cpp
    g729/*.c
    utils/*.cpp
)


include_directories (
	${PROJECT_SOURCE_DIR}/src
	${PROJECT_SOURCE_DIR}/src/g729
	${PROJECT_SOURCE_DIR}/src/base
	${PROJECT_SOURCE_DIR}/src/base/ipc
	${PROJECT_SOURCE_DIR}/src/utils
)


link_libraries (
    pthread
	swscale
	swresample
	postproc
	avutil
	avformat
	avfilter
	avdevice
	avcodec
    bctoolbox
) 


add_library(offlineRtpSdk STATIC SHARED ${SOURCE_FILES})

set_target_properties(offlineRtpSdk PROPERTIES LINKER_LANGUAGE CXX)
set_target_properties(offlineRtpSdk PROPERTIES PREFIX "lib" DEBUG_POSTFIX "d" OUTPUT_NAME "offlineRtpSdk")