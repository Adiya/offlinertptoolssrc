//
//  InnerFormatTransfer.h
//  MediaRecordDemo
//
//  Created by xqm on 2017/8/16.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef InnerFormatTransfer_h
#define InnerFormatTransfer_h

#include "MediaFormat.h"

extern "C"
{
#include "libavutil/pixfmt.h"
#include "libavutil/samplefmt.h"
}

static enum AVPixelFormat getVideoInnerFormat(enum VIDEO_PIX_FORMAT outFormat)
{
    switch (outFormat) {
        case MT_PIX_FMT_YUV420P:
            return AV_PIX_FMT_YUV420P;
        case MT_PIX_FMT_YUV422P:
            return AV_PIX_FMT_YUV422P;
        case MT_PIX_FMT_YUV444P:
            return AV_PIX_FMT_YUV444P;
        case MT_PIX_FMT_NV12:
            return AV_PIX_FMT_NV12;
        case MT_PIX_FMT_NV21:
            return AV_PIX_FMT_NV21;
        case MT_PIX_FMT_RGBA:
            return AV_PIX_FMT_RGBA;
        default:
            return AV_PIX_FMT_NONE;
    }
}

static enum VIDEO_PIX_FORMAT getVideoOuterFormat(enum AVPixelFormat inFormat)
{
    switch (inFormat) {
            case AV_PIX_FMT_YUV420P:
            return MT_PIX_FMT_YUV420P;
            case AV_PIX_FMT_YUV422P:
            return MT_PIX_FMT_YUV422P;
            case AV_PIX_FMT_YUV444P:
            return MT_PIX_FMT_YUV444P;
            case AV_PIX_FMT_NV12:
            return MT_PIX_FMT_NV12;
            case AV_PIX_FMT_NV21:
            return MT_PIX_FMT_NV21;
        default:
            return MT_PIX_FMT_NONE;
    }
}

static enum AVSampleFormat getAudioInnerFormat(enum AUDIO_SAMPLE_FORMAT outFormat)
{
    switch (outFormat) {
        case MT_SAMPLE_FMT_U8:
            return AV_SAMPLE_FMT_U8;
        case MT_SAMPLE_FMT_S16:
            return AV_SAMPLE_FMT_S16;
        case MT_SAMPLE_FMT_S32:
            return AV_SAMPLE_FMT_S32;
        case MT_SAMPLE_FMT_FLT:
            return AV_SAMPLE_FMT_FLT;
        case MT_SAMPLE_FMT_DBL:
            return AV_SAMPLE_FMT_DBL;
        case MT_SAMPLE_FMT_U8P:
            return AV_SAMPLE_FMT_U8P;
        case MT_SAMPLE_FMT_S16P:
            return AV_SAMPLE_FMT_S16P;
        case MT_SAMPLE_FMT_S32P:
            return AV_SAMPLE_FMT_S32P;
        case MT_SAMPLE_FMT_FLTP:
            return AV_SAMPLE_FMT_FLTP;
        default:
            return AV_SAMPLE_FMT_NONE;
    }
}

static enum AUDIO_SAMPLE_FORMAT getAudioOuterFormat(enum AVSampleFormat outFormat)
{
    switch (outFormat) {
            case AV_SAMPLE_FMT_U8:
            return MT_SAMPLE_FMT_U8;
            case AV_SAMPLE_FMT_S16:
            return MT_SAMPLE_FMT_S16;
            case AV_SAMPLE_FMT_S32:
            return MT_SAMPLE_FMT_S32;
            case AV_SAMPLE_FMT_FLT:
            return MT_SAMPLE_FMT_FLT;
            case AV_SAMPLE_FMT_DBL:
            return MT_SAMPLE_FMT_DBL;
            case AV_SAMPLE_FMT_U8P:
            return MT_SAMPLE_FMT_U8P;
            case AV_SAMPLE_FMT_S16P:
            return MT_SAMPLE_FMT_S16P;
            case AV_SAMPLE_FMT_S32P:
            return MT_SAMPLE_FMT_S32P;
            case AV_SAMPLE_FMT_FLTP:
            return MT_SAMPLE_FMT_FLTP;
        default:
            return MT_SAMPLE_FMT_NONE;
    }
}


#endif /* InnerFormatTransfer_h */
