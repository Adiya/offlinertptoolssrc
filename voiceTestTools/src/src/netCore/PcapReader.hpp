//
//  PcapReader.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef PcapReader_hpp
#define PcapReader_hpp

#include <stdio.h>

#include <string>

#include "pcap/pcap.h"

#ifdef MAX_DEVICE_NUM
#define MAX_DEVICE_NUM 4
#else
#undef MAX_DEVICE_NUM
#define MAX_DEVICE_NUM 4
#endif

class TransmitCenter;

class PcapReader {
public:
    PcapReader();
    ~PcapReader();
    
    /** 打开pcap包
     *
     * @param filename 文件名
     * @return ==0 为正常; < 0为异常
     */
    bool open(const char *filename);

    /** 关闭句柄
     *
     */
    void close();
    
    /** 设置过滤条件
     *
     * @param filter 过滤条件，支持正则表达式
     * @return true 为正常; false为异常
     */
    bool setFilter(const char *filter);
    
    /** 读取数据包，同步方式
     *
     * @param pktHeader 报文头信息
     * @param data 数据
     * @return ==0 为正常; < 0为异常
     */
    int readPacket(pcap_pkthdr **pktHeader, u_int8_t **data);
    
    /** 配置网卡
     *
     * @param eth 网卡名称
     * @return ==0 为正常; < 0为异常
     */
    bool setEthernet(const char *eth);
    
    bool openAllEthernet();
    
    bool closeAllEthernet();
    
    bool catchEthernet(int index);
    
    bool serchAllDev();
    
    int setLog(int isLog);
    
    int setLimitIp(std::string ip);
    
    int setServer(std::string serveraddr, int port);
    
private:
    
    TransmitCenter *transmitCenter;
    
    pcap_if_t *allPcapDevs; // 所有能抓包设备
    
    char errbuf[PCAP_ERRBUF_SIZE];
    
    pcap_t *handles[MAX_DEVICE_NUM];
    
    std::string limitIp;
    
    std::string serverAddr;
    int serverPort;
    
    int netCnt;
    
    pcap_t *handle;
    
};


#endif /* PcapReader_hpp */
