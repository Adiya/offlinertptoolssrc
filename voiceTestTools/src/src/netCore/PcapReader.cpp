//
//  PcapReader.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include <arpa/inet.h>
#include <string.h>
#include <netinet/if_ether.h>

#include "TransmitCenter.hpp"
#include "PcapReader.hpp"

#include "CnUtils.h"

#define SNAP_LEN 65536

PcapReader::PcapReader()
{
    handle = NULL;
    transmitCenter = NULL;
    netCnt = 0;
}

PcapReader::~PcapReader()
{
    
}

/** 打开pcap包
 *
 * @param filename 文件名
 * @return ==0 为正常; < 0为异常
 */
bool PcapReader::open(const char *filename)
{
    char error[128];
    
    if (handle) {
        av_log(NULL, AV_LOG_WARNING, "File has opened!\n");
        pcap_close(handle);
    }
    
    handle = pcap_open_offline(filename, error);
    if (handle == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Open offline file errror![%s]\n", error);
        return false;
    }
    
    return true;
}

void PcapReader::close()
{
    if (handle) {
        pcap_close(handle);
        handle = NULL;
    }
}

/** 设置过滤条件
 *
 * @param filterStr 过滤条件，支持正则表达式
 * @return true 为正常; false为异常
 */
bool PcapReader::setFilter(const char *filterStr)
{
    int ret;
    struct bpf_program filter;

    ret = pcap_compile(handle, &filter, filterStr, 1, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "compile filter bpf error!\n");
        return false;
    }
    
    ret = pcap_setfilter(handle, &filter);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Set filter error!\n");
        return false;
    }
    
    av_log_set_level(AV_LOG_TRACE);

    return true;
}

static long packetCnt = 0;

/** 读取数据包，同步方式
 *
 * @param pktHeader 报文头信息
 * @param data 数据
 * @return ==0 为正常; < 0为异常
 */
int PcapReader::readPacket(pcap_pkthdr **pktHeader, u_int8_t **data)
{
    int ret;
    
    ret = pcap_next_ex(handle, pktHeader, (const u_char **)data);
    if (ret == 1) {
        packetCnt++;
        av_log(NULL, AV_LOG_TRACE, "[%ld]Read a packet,[len=%d caplen=%d]\n", packetCnt, (*pktHeader)->len,(*pktHeader)->caplen);
    } else if (ret < 0) {
        return ret;
    } else {
        av_log(NULL, AV_LOG_ERROR, "Read size is 0\n");
    }
    
    return ret;
}

/** 配置网卡
 *
 * @param eth 网卡名称
 * @return ==0 为正常; < 0为异常
 */
bool PcapReader::setEthernet(const char *eth)
{
    return true;
}

bool PcapReader::openAllEthernet()
{
    struct bpf_program fp1;
    pcap_if_t *pAllPcapDev;
    char *dev;
//    int i = 0;
    int ret;
    char filter_exp[] = "udp || udp[8] & 0x80 == 0x80";
    
    ret = pcap_findalldevs(&allPcapDevs, errbuf);
    if (ret < 0) {
        av_log(NULL, AV_LOG_WARNING, "Cannot find ethernet device!\n");
        return false;
    }
    
    pAllPcapDev = allPcapDevs;
    while (pAllPcapDev != NULL) {
        dev = pAllPcapDev->name;
        av_log(NULL, AV_LOG_DEBUG, "dev name %s flag: %d\n", dev, pAllPcapDev->flags);
        if (netCnt >= MAX_DEVICE_NUM) {
            av_log(NULL, AV_LOG_WARNING, "More than max device numbers\n");
            break;
        }
        handles[netCnt] = pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
        if (handles[netCnt] == NULL) {
            fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
            break;
        }
        printf("%d open dev name %s\n", netCnt, dev);
        if (pcap_datalink(handles[netCnt]) != DLT_EN10MB) {
            fprintf(stderr, "%s is not an Ethernet\n", dev);
            break;
        }
        
        /* compile the filter expression */
        if (pcap_compile(handles[netCnt], &fp1, filter_exp, 0, 24) == -1) {
            fprintf(stderr, "Couldn't parse filter %s: %s\n",
                    filter_exp, pcap_geterr(handles[netCnt]));
            break;
        }
        /* apply the compiled filter */
        if (pcap_setfilter(handles[netCnt], &fp1) == -1) {
            fprintf(stderr, "Couldn't install filter %s: %s\n",
                    filter_exp, pcap_geterr(handles[netCnt]));
            break;
        }
        netCnt++;
        pAllPcapDev = pAllPcapDev->next;
    }

    transmitCenter = new TransmitCenter();
    if (transmitCenter == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Create transmit center error!\n");
    }
    
    transmitCenter->init();
    
    transmitCenter->setLimitIp(limitIp);
    transmitCenter->setServer(serverAddr, serverPort);
    
    av_log(NULL, AV_LOG_TRACE, "Create transmitCenter [%p]\n", transmitCenter);
    
    return true;
}

int PcapReader::setLimitIp(std::string ip)
{
    limitIp = ip;
    
    return 0;
}

int PcapReader::setServer(std::string addr, int port)
{
    serverAddr = addr;
    serverPort = port;
    
    return 0;
}

void getPacket(u_char *arg, const struct pcap_pkthdr * pkthdr, const u_char * packet)
{
    if (arg == NULL) {
        av_log(NULL, AV_LOG_WARNING, "argument is null\n");
        return;
    }
    
    TransmitCenter *transmitCenter = (TransmitCenter *)arg;
    
    bool ret = transmitCenter->import(pkthdr, packet);
    if (ret == false) {
        av_log(NULL, AV_LOG_WARNING, "transmit data error!\n");
    }
}

bool PcapReader::catchEthernet(int index)
{
    av_log(NULL, AV_LOG_INFO, "Begin open %d ethernet\n", index);
    if (index >= 0 && index < MAX_DEVICE_NUM) {
        av_log(NULL, AV_LOG_INFO, "[%p]Open net card index %d\n", transmitCenter, index);
        pcap_loop(handles[index], -1, getPacket, (u_char*)transmitCenter);
    }
    
    return false;
}

#define PCAP_DEBUG_MODEL 1

int PcapReader::setLog(int isLog)
{
    if (isLog) {
        av_log_set_level(AV_LOG_TRACE);
    } else {
        av_log_set_level(AV_LOG_FATAL);
    }
    return 0;
}

bool PcapReader::serchAllDev()
{
    int ret;
    char errbuf[128];
#if PCAP_DEBUG_MODEL
    pcap_if_t *pAllPcapDev;
#endif
    pcap_if_t *allPcapDev;
    
    ret = pcap_findalldevs(&allPcapDev, errbuf);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Find devs error![%s]\n", errbuf);
        return false;
    }
    
#if PCAP_DEBUG_MODEL
    if (ret == 0) {
        int i = 0;
        pAllPcapDev = allPcapDev;
        while(!(pAllPcapDev == NULL)) {
            printf("设备 %d 的名称 %s\n", i, pAllPcapDev->name);
            printf("设备 %d 的描述 %s flag %d\n", i, pAllPcapDev->description, pAllPcapDev->flags);
            pAllPcapDev=pAllPcapDev->next;
            i++;
        }
    }
    
#endif
    
    return true;
}

bool PcapReader::closeAllEthernet()
{
    if (allPcapDevs) {
        pcap_freealldevs(allPcapDevs);
        return true;
    } else {
        return false;
    }
}
