//
//  TcpParser.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/23.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "TcpParser.hpp"

int parserTcp(TcpHeader_t **pTcpHeader, const u_int8_t *tcpData)
{
    //dummy code
    TcpHeader_t *tcp_protocol;
    
    tcp_protocol = (TcpHeader_t *)(tcpData);
#if 0
    u_short source_port = ntohs(tcp_protocol->sport);
    u_short destination_port = ntohs(tcp_protocol->dport);
    u_int sequence = ntohl(tcp_protocol->sn);
    u_int acknowledgement = ntohl(tcp_protocol->ack);
    u_char flags = tcp_protocol->th_flags;
    u_int16_t win_size = ntohl(tcp_protocol->winSize);
    u_int16_t checksum = ntohl(tcp_protocol->checkSum);
#endif
    return 0;
}
