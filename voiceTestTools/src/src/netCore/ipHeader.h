//
//  ipHeader.h
//  catchNetPacket
//
//  Created by Adiya on 2018/11/29.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef ipHeader_h
#define ipHeader_h

#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <netinet/if_ether.h>

typedef struct ip_header {
    u_int8_t ip_version: 4;
    u_int8_t ip_header_length:4;
    u_int8_t ip_tos;
    u_int16_t ip_length;
    u_int16_t ip_id;
    u_int16_t ip_off;
    u_int8_t ip_ttl;
    u_int8_t ip_protocol;
    u_int16_t ip_checksum;
    
    struct in_addr ip_source_address;
    struct in_addr ip_destination_address;
} ip_header;


#endif /* ipHeader_h */
