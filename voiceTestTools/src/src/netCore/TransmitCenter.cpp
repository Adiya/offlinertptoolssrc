//
//  TransmitCenter.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//
#include <netinet/if_ether.h>

#include "TransmitCenter.hpp"
#include "ProtocolParser.hpp"
#include "ipHeader.h"
#include "CnUtils.h"
#include "UdpParser.hpp"
#include "VoiceprintDetectionTask.hpp"
#include "MissionContral.hpp"
#include "g729a.h"


TransmitCenter::TransmitCenter()
{
    missionCtr = NULL;
}

TransmitCenter::~TransmitCenter()
{
    
}

bool TransmitCenter::init(void)
{
    if (missionCtr == NULL) {
        missionCtr = new MissionContral();
    } else {
        av_log(NULL, AV_LOG_WARNING, "Reinit\n");
        return false;
    }
    
    
    va_g729a_init_decoder();
    
    return true;
}

void TransmitCenter::setLimitIp(std::string ipstr)
{
    limitIp = ipstr;
}

int parserIp(struct ip_header **pheader, const u_int8_t* packetData)
{
    u_char *ipData;
    u_int16_t *pshortVersionData;
    char data;
    u_int header_length;
    u_int offset;
    u_char tos;
    u_int16_t checksum;
    struct ip_header *header;;
    
    header = (struct ip_header*)(packetData);
    ipData = (u_char *)(packetData);
    
    pshortVersionData = (u_int16_t *)ipData;
    data = ipData[0];
    header->ip_version = data>>4;
    header->ip_header_length = data & 0x0F;
    
    checksum = ntohs(header->ip_checksum);
    header_length = header->ip_header_length * 4;
    tos = header->ip_tos;
    
    offset = ntohs(header->ip_off);
    
    *pheader = header;
    
    return 0;
}


//FILE *g729fp = NULL;
//const char *g729filanem = "/Users/xqm/Documents/testdir/audio/g729DataSet/out1.g729";
//const char *file = "122.19.109.208-122.19.109.170-29266_18.g729";
static int seqNumBase = 0;

bool TransmitCenter::import(const pcap_pkthdr *pktHeader, const u_int8_t *packetData)
{
    long ts;
    struct ether_header *ethernet_protocol;
    struct ip_header *ip_protocol;
    u_int8_t *p_ipData = NULL;
    int pktLen = pktHeader->caplen;
    bool isVaildProtocol = false;
    int ipDataLen = 0;
    int netDataLen;

    ts = pktHeader->ts.tv_sec * 1000000 + pktHeader->ts.tv_usec;
    
    ethernet_protocol = (struct ether_header*)packetData;
    u_short ethernet_type = (u_short)ntohs(ethernet_protocol->ether_type);
    
#if 0
    if (g729fp == NULL) {
        g729fp = fopen(g729filanem, "w+");
    }
#endif
    
    av_log(NULL, AV_LOG_INFO, "import data len %d\n", pktHeader->caplen);
    
    switch (ethernet_type) {
        case ETHERTYPE_VLAN:
            av_log(NULL, AV_LOG_TRACE, "VLAN protocol\n");
            p_ipData = (u_int8_t *)(packetData + VTHERNET_HEADER + VLAN_HEARER);
            ipDataLen = pktLen - (VTHERNET_HEADER + VLAN_HEARER);
            isVaildProtocol = true;
            break;
            
        case ETHERTYPE_IP:
            av_log(NULL, AV_LOG_TRACE, "IP protocol\n");
            ipDataLen = pktLen - VTHERNET_HEADER;
            p_ipData = (u_int8_t *)(packetData + VTHERNET_HEADER);
            isVaildProtocol = true;
            break;
            
        default:
//            av_log(NULL, AV_LOG_INFO, "This ethernet type cannot support![%d]\n", ethernet_type);
            break;
    }
    
    if (isVaildProtocol) {
        char addr_p[20];
        char addrdst_p[20];
        const char *srcIp = NULL;
        const char *dstIp = NULL;
        TcpHeader_t *tcpHeader;
        UdpHeader_t *udpHeader;
        u_int8_t *pNetData;
        int payload = -1;
        int seqNum;
        int ret;
        
        parserIp(&ip_protocol, p_ipData);
        srcIp = inet_ntop((int)AF_INET, (const void *)(&ip_protocol->ip_source_address), (char *)addr_p, (socklen_t)sizeof(addr_p));
        dstIp = inet_ntop((int)AF_INET, (const void *)(&ip_protocol->ip_destination_address), (char *)addrdst_p, (socklen_t)sizeof(addr_p));
        
        if (limitIp.length() > 0) {
            if (strcmp(dstIp, limitIp.c_str()) != 0) {
                return false;
            }
        }

        av_log(NULL, AV_LOG_INFO, "srcIp %s dstIp %s\n", srcIp, dstIp);
        
        pNetData = (u_int8_t *)(p_ipData + IP_HEADER_LEN);
        
        switch (ip_protocol->ip_protocol) {
            case 6: // tcp
                av_log(NULL, AV_LOG_VERBOSE, "It is tcp \n");
                netDataLen = ipDataLen - IP_HEADER_LEN - TCP_HEADER_LEN;
                parserTcp(&tcpHeader, pNetData);
                break;
                
            case 17: // udp
                char taskid[2048];
                u_short destination_port;
                av_log(NULL, AV_LOG_VERBOSE, "It is udp \n");
                VoiceprintDetectionTask *task;
                netDataLen = ipDataLen - IP_HEADER_LEN - UDP_HEADER_LEN;
                parserUdp(&udpHeader, pNetData);
                
                if (netDataLen < RTP_HEADER_LEN) {
                    av_log(NULL, AV_LOG_WARNING, "It is not vaild rtp protocol\n");
                    return false;
                }
                
                ret = parserRtp(pNetData + UDP_HEADER_LEN, RTP_HEADER_LEN, &payload, &seqNum);
                if (ret < 0) {
                    return false;
                }

                destination_port = ntohs(udpHeader->dstPort);
                av_log(NULL, AV_LOG_INFO, "payload type %d seqNum %d dst port %d\n", payload, seqNum, destination_port);
                
                if (18 != payload) {
                    return false;
                }
                
                snprintf(taskid, sizeof(taskid), "%s-%s-%d_%d", srcIp, dstIp, destination_port, payload);
                
                task = missionCtr->getTask(taskid);
                if (task == NULL || (task && ((seqNum == 1) && (seqNum < task->getSeqNum())))) {
                    if (task) {
                        missionCtr->taskUnregister(task);
                        task->stop();
                        delete task;
                    }
                    task = new VoiceprintDetectionTask();
                    if (task == NULL) {
                        av_log(NULL, AV_LOG_ERROR, "Create task error!\n");
                        return false;
                    }
                    ret = task->setTaskId(taskid);
                    if (ret < 0) {
                        av_log(NULL, AV_LOG_ERROR, "Set task id is vaild\n");
                        return false;
                    }
                    task->setServerInfo(audioServer, serverPort);
                    missionCtr->taskRegister(task);
                    task->start();
                }
                
                if (seqNum > task->getSeqNum()) {
                    task->setSeqNum(seqNum);
                    task->sendData(pNetData + UDP_HEADER_LEN + RTP_HEADER_LEN, netDataLen - RTP_HEADER_LEN);
                }
                
#if 0
//                if (g729fp && (strcmp(file, taskid) == 0)) {
//                    if (seqNumBase < seqNum) {
//                        seqNumBase = seqNum;
//                        fwrite(pNetData + UDP_HEADER_LEN + RTP_HEADER_LEN, netDataLen - RTP_HEADER_LEN, 1, g729fp);
//                    } else {
//
//                    }
//                }
#endif
                break;
            default:
                return false;
        }
        
    } else {
        return false;
    }
    
    return true;
}

int TransmitCenter::setServer(std::string server, int port)
{
    audioServer = server;
    serverPort = port;
    
    return 0;
}
