//
//  PcapApp.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/21.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "PcapApp.hpp"
#include "CnUtils.h"

PcapApp::PcapApp()
{
    
}

PcapApp::~PcapApp()
{
    
}

bool PcapApp::setOption(PcapOption_t *options)
{
    if (!checkOption(options)) {
        return false;
    }
    
    return true;
}


bool PcapApp::checkOption(PcapOption_t *o)
{
    if (o == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Option is null\n");
        return false;
    }
    
    if (o->serverAdd.empty() || o->serverPort <= 0) {
        av_log(NULL, AV_LOG_ERROR, "Server configure is invailid![addr=%s:%d]\n", \
               o->serverAdd.c_str(), o->serverPort);
        return false;
    }
    
    if (o->localCapFile.empty() && o->netCardIndex <= 0) {
        av_log(NULL, AV_LOG_ERROR, "Unknown catch pcap ways!\n");
        return false;
    }
    
    return true;
}
