//
//  UdpParser.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "UdpParser.hpp"
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <netinet/if_ether.h>

#include "CnUtils.h"

int parserUdp(UdpHeader_t **pUdp_protocol, const u_int8_t *udpData)
{
    UdpHeader_t *udp_protocol;
    
    udp_protocol = (UdpHeader_t *)(udpData);
//    
//    u_short source_port = ntohs(udp_protocol->srcPort);
//    u_short destination_port = ntohs(udp_protocol->dstPort);
//    u_int userHeaderLen = ntohl(udp_protocol->uhl);
//    u_int16_t checksum = ntohl(udp_protocol->chkSum);
    
//    av_log(NULL, AV_LOG_VERBOSE, "source port %d dst port %d userHeaderLen %d checksum %d\n", source_port, \
           destination_port, userHeaderLen, checksum);
    *pUdp_protocol = udp_protocol;
    
    return 0;
}

int parserRtp(const u_int8_t *rtpData, int rtpDataLen, int *payload, int *seqNum)
{
    if (rtpDataLen < 12) {
        //Too short to be a valid RTP header.
        return -1;
    }
    
    if ((rtpData[0] >> 6) != 2) {
        //Currently, the version is 2, if is not 2, unsupported.
        return -1;
    }
    
    if (rtpData[0] & 0x20) {
        // Padding present.
        size_t paddingLength = rtpData[rtpDataLen - 1];
        if (paddingLength + 12 > rtpDataLen) {
            return -1;
        }
        rtpDataLen -= paddingLength;
    }
    
    int numCSRCs = rtpData[0] & 0x0f;
    size_t payloadOffset = 12 + 4 * numCSRCs;
    
    if (rtpDataLen < payloadOffset) {
        // Not enough rtpData to fit the basic header and all the CSRC entries.
        return -1;
    }
    
    if (rtpData[0] & 0x10) {
        // Header extension present.
        if (rtpDataLen < payloadOffset + 4) {
            // Not enough rtpData to fit the basic header, all CSRC entries and the first 4 bytes of the extension header.
            return -1;
        }
        
        const uint8_t *extensionData = &rtpData[payloadOffset];
        size_t extensionLength = 4 * (extensionData[2] << 8 | extensionData[3]);
        
        if (rtpDataLen < payloadOffset + 4 + extensionLength) {
            return -1;
        }
        payloadOffset += (4 + extensionLength);
    }
    
    *payload = rtpData[1];
    
    uint32_t rtpTime = rtpData[4] << 24 | rtpData[5] << 16 | rtpData[6] << 8 | rtpData[7];
    
    uint32_t srcId = rtpData[8] << 24 | rtpData[9] << 16 | rtpData[10] << 8 | rtpData[11];
    
    *seqNum = rtpData[2] << 8 | rtpData[3];
    
    return 0;
}
