//
//  UdpParser.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef UdpParser_hpp
#define UdpParser_hpp

#include <stdio.h>
#include <sys/types.h>

typedef struct UdpHeader
{
    unsigned short srcPort; //远端口号
    unsigned short dstPort; //目的端口号
    unsigned short uhl;      //udp头部长度
    unsigned short chkSum;  //16位udp检验和
} UdpHeader_t;

int parserUdp(UdpHeader_t **pUdp_protocol, const u_int8_t *udpData);


int parserRtp(const u_int8_t *rtpData, int rtpDataLen, int *payload, int *seqNum);


#endif /* UdpParser_hpp */
