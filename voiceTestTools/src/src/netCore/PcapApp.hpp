//
//  PcapApp.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/21.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef PcapApp_hpp
#define PcapApp_hpp

#include <stdio.h>

#include <iostream>

typedef struct PcapOption_t {
    std::string serverAdd;
    int serverPort;
    int netCardIndex;
    std::string limitIp;
    std::string localCapFile;
    std::string saveDir;
    int log;
} PcapOption_t;


class PcapApp {
public:
    PcapApp();
    ~PcapApp();
    
    /** 配置输入配置项
     *
     * @param options 配置项
     * @return true 为正常; false为异常
     */
    bool setOption(PcapOption_t *options);
    
private:
    
    /** 测试输入配置项
     *
     * @param o 配置项
     * @return true 为正常; false为异常
     */
    bool checkOption(PcapOption_t *o);
    
private:
    
    PcapOption_t *gobalOption;
    
};


#endif /* PcapApp_hpp */
