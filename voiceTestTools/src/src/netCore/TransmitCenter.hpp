//
//  TransmitCenter.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef TransmitCenter_hpp
#define TransmitCenter_hpp

#include <stdio.h>
#include <string>
#include <map>

#include "pcap/pcap.h"

class VoiceprintDetectionTask;
class ProtocolParser;
class MissionContral;

class TransmitCenter {
public:
    TransmitCenter();
    ~TransmitCenter();
    
    /** 初始化
     *
     * @return true 为正常; false 为异常
     */
    bool init(void);
    
    
    void setLimitIp(std::string ip);
    
    /** 打开pcap包
     *
     * @param pktHeader 报文的头部信息
     * @param data      数据
     * @return true 为正常; false 为异常
     */
    bool import(const pcap_pkthdr *pktHeader, const u_int8_t *data);
    
    /** 获取目前处理的任务数
     *
     * @return > 0 为任务树木; == 0 无任务数
     */
    uint32_t getTaskNums(void);
    
    int setServer(std::string server, int port);
    
private:
    
    ProtocolParser *parser;                             // 协议解析类
    
    std::string limitIp;
    
    std::string audioServer;
    int serverPort;
    
    MissionContral *missionCtr;
    
//    std::map<std::string, VoiceprintDetectionTask *> taskMap;   // 任务表
};

#endif /* TransmitCenter_hpp */
