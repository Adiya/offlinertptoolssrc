//
//  TcpParser.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/23.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef TcpParser_hpp
#define TcpParser_hpp

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

typedef struct TcpHeader {
    u_int16_t sport;
    u_int16_t dport;
    u_int32_t sn;
    u_int32_t ack;
    //  u_int16_t other;
#if BYTE_ORDER == LITTLE_ENDIAN
    u_char th_x2:4,
th_off:4;
#elif BYTE_ORDER == BIG_ENDIAN
    u_char th_off:4,
th_x2:4;
#endif
    u_char th_flags;
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
    u_int16_t win_size;
    u_int16_t checksum;
    u_int16_t urg_ptr;
} TcpHeader_t;

int parserTcp(TcpHeader_t **pTcpHeader, const u_int8_t *tcpData);

#endif /* TcpParser_hpp */
