//
//  ProtocolParser.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/22.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef ProtocolParser_hpp
#define ProtocolParser_hpp

#include <stdio.h>

#include "UdpParser.hpp"
#include "TcpParser.hpp"

#ifndef UDP_HEADER_LEN
#define UDP_HEADER_LEN 8
#else
#undef UDP_HEADER_LEN
#define UDP_HEADER_LEN 8
#endif

#ifndef TCP_HEADER_LEN
#define TCP_HEADER_LEN 20
#else
#undef TCP_HEADER_LEN
#define TCP_HEADER_LEN 20
#endif

#ifndef VLAN_HEARER
#define VLAN_HEARER 4
#else
#undef VLAN_HEARER
#define VLAN_HEARER 4
#endif

#ifndef VTHERNET_HEADER
#define VTHERNET_HEADER 14
#else
#undef VTHERNET_HEADER
#define VTHERNET_HEADER 14
#endif


#ifndef IP_HEADER_LEN
#define IP_HEADER_LEN 20
#else
#undef IP_HEADER_LEN
#define IP_HEADER_LEN 20
#endif

#ifndef RTP_HEADER_LEN
#define RTP_HEADER_LEN 12
#else
#undef RTP_HEADER_LEN
#define RTP_HEADER_LEN 12
#endif


class ProtocolParser {
public:
    ProtocolParser();
    ~ProtocolParser();
    
    void setZeroMemCopy(bool zeroMemCopy = true);
    
    bool import(const u_int8_t *data, size_t dataLen);
    
    u_int8_t *getPayload(int &payloadType, size_t &payloadSize);
    
    int setTs();
    
private:
    
    int ipParser(const u_int8_t *packetData, const size_t packetDataLen);
    
private:
    
    bool zeroMemCopyFlag;
    
    u_int8_t *m_payload;
    int m_payloadType;
    int m_payloadSize;
};


#endif /* ProtocolParser_hpp */
