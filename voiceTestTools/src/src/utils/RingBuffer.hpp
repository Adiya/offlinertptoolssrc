//
//  RingBuffer.hpp
//  libpcap_app
//
//  Created by Adiya on 2018/10/25.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef RingBuffer_hpp
#define RingBuffer_hpp

#include <stdio.h>

#include <pthread.h>

enum {
    RING_IDEL,
    RING_RUNING,
    RING_STOP,
};

struct RingBuffer {
    unsigned char *buffer;
    unsigned int size;
    unsigned int in;
    unsigned int out;
    int stat;
    pthread_mutex_t *lock;
};

extern struct RingBuffer *ring_init(unsigned char *buffer, unsigned int size,pthread_mutex_t *lock);
extern struct RingBuffer *ring_alloc(unsigned int size,pthread_mutex_t *lock);
extern void ring_free(struct RingBuffer *fifo);
extern unsigned int __ring_put(struct RingBuffer *fifo,
                               unsigned char *buffer, unsigned int len);
extern unsigned int __ring_get(struct RingBuffer *fifo,
                               unsigned char *buffer, unsigned int len);


static  void __ring_reset(struct RingBuffer *fifo)
{
    fifo->in = fifo->out = 0;
}

static  void ring_reset(struct RingBuffer *fifo)
{
    unsigned long flags;
    
    pthread_mutex_lock(fifo->lock);
    
    __ring_reset(fifo);
    
    pthread_mutex_unlock(fifo->lock);
}

static  unsigned int __ring_len(struct RingBuffer *fifo)
{
    return fifo->in - fifo->out;
}

static  unsigned int ring_len(struct RingBuffer *fifo)
{
    unsigned int ret;
    
    pthread_mutex_lock(fifo->lock);

    ret = __ring_len(fifo);
    
    pthread_mutex_unlock(fifo->lock);
    
    return ret;
}

static unsigned int ring_put(struct RingBuffer *fifo,
                                    unsigned char *buffer, unsigned int len)
{
    unsigned int ret;
    
    pthread_mutex_lock(fifo->lock);
    
    fifo->stat = RING_RUNING;
    
    ret = __ring_put(fifo, buffer, len);
    
    pthread_mutex_unlock(fifo->lock);
    
    return ret;
}


static unsigned int ring_get(struct RingBuffer *fifo,
                                    unsigned char *buffer, unsigned int len)
{
    unsigned int ret;
    
    pthread_mutex_lock(fifo->lock);
    
    ret = __ring_get(fifo, buffer, len);
    
    if (fifo->in == fifo->out)
        fifo->in = fifo->out = 0;
    
    pthread_mutex_unlock(fifo->lock);
    
    return ret;
}

#endif /* RingBuffer_hpp */
