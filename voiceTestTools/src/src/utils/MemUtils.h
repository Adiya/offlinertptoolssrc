//
//  MemUtils.h
//  readFrame
//
//  Created by xqm on 2018/3/15.
//  Copyright © 2018年 xqm. All rights reserved.
//

#ifndef MemUtils_h
#define MemUtils_h

//#define SAFE_FREE(P)   do{free(p); p = NULL;}while(0)
#ifndef SAFE_FREE
#define SAFE_FREE(ptr) do{ if(ptr != NULL) { free(ptr); ptr = NULL; }} while(0)
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(ptr) do{ if(ptr != NULL) { delete(ptr); ptr = NULL; }} while(0)
#endif

#define MT_SET_BIT(x,n) (x|(1U<<(n-1)))
#define MT_GET_BIT(x,n) (x&(1U<<(n-1)))
#define MT_CLEAR_BIT(x,n) (x&~(1U<<(n-1)))
#define MT_SET_BIT_N_M(x,n,m) (x|((~((~0U)<<(m-n+1)))<<(n-1)))
#define MT_CLEAR_BIT_N_M(x,n,m) (x&~((~((~0U)<<(m-n+1)))<<(n-1)))
#define MT_GET_BITS(x,n,m) (x&((~((~0U)<<(m-n+1)))<<(n-1)))>>(n-1)

void *growArray(void *array, int elem_size, int *size, int new_size);

#endif /* MemUtils_h */
