//
//  MTLog.h
//  mmtools
//
//  Created by xqm on 2018/5/2.
//  Copyright © 2018年 xqm. All rights reserved.
//

#ifndef MTLog_h
#define MTLog_h

#ifdef __ANDROID__

#include <android/log.h>

#define MT_LOG_UNKNOWN     ANDROID_LOG_UNKNOWN
#define MT_LOG_DEFAULT     ANDROID_LOG_DEFAULT

#define MT_LOG_VERBOSE     ANDROID_LOG_VERBOSE
#define MT_LOG_DEBUG       ANDROID_LOG_DEBUG
#define MT_LOG_INFO        ANDROID_LOG_INFO
#define MT_LOG_WARN        ANDROID_LOG_WARN
#define MT_LOG_ERROR       ANDROID_LOG_ERROR
#define MT_LOG_FATAL       ANDROID_LOG_FATAL
#define MT_LOG_SILENT      ANDROID_LOG_SILENT

#define VLOG(level, TAG, ...)    ((void)__android_log_vprint(level, TAG, __VA_ARGS__))
#define ALOG(level, TAG, ...)    ((void)__android_log_print(level, TAG, __VA_ARGS__))

#else

#define MT_LOG_UNKNOWN     0
#define MT_LOG_DEFAULT     1

#define MT_LOG_VERBOSE     2
#define MT_LOG_DEBUG       3
#define MT_LOG_INFO        4
#define MT_LOG_WARN        5
#define MT_LOG_ERROR       6
#define MT_LOG_FATAL       7
#define MT_LOG_SILENT      8

#define VLOG(level, TAG, ...)    ((void)vprintf(__VA_ARGS__))
#define ALOG(level, TAG, ...)    ((void)printf(__VA_ARGS__))

#endif

#define MT_LOG_TAG "MTMEDIA"

#define VLOGV(...)  VLOG(MT_LOG_VERBOSE,   MT_LOG_TAG, __VA_ARGS__)
#define VLOGD(...)  VLOG(MT_LOG_DEBUG,     MT_LOG_TAG, __VA_ARGS__)
#define VLOGI(...)  VLOG(MT_LOG_INFO,      MT_LOG_TAG, __VA_ARGS__)
#define VLOGW(...)  VLOG(MT_LOG_WARN,      MT_LOG_TAG, __VA_ARGS__)
#define VLOGE(...)  VLOG(MT_LOG_ERROR,     MT_LOG_TAG, __VA_ARGS__)

#define ALOGV(...)  ALOG(MT_LOG_VERBOSE,   MT_LOG_TAG, __VA_ARGS__)
#define ALOGD(...)  ALOG(MT_LOG_DEBUG,     MT_LOG_TAG, __VA_ARGS__)
#define ALOGI(...)  ALOG(MT_LOG_INFO,      MT_LOG_TAG, __VA_ARGS__)
#define ALOGW(...)  ALOG(MT_LOG_WARN,      MT_LOG_TAG, __VA_ARGS__)
#define ALOGE(...)  ALOG(MT_LOG_ERROR,     MT_LOG_TAG, __VA_ARGS__)
#define LOG_ALWAYS_FATAL(...)   do { ALOGE(__VA_ARGS__); exit(1); } while (0)

#endif /* MTLog_h */
