//
//  RtpConn.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/13.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "RtpConn.hpp"
#include "RtpPayload.h"

RtpConn::RtpConn()
{
    m_curTimeStamp = 0;
}

RtpConn::~RtpConn()
{
    
}

int RtpConn::create(const char *dstIp, int dstPort, int payload)
{
    int ret = 0;
    char *ssrc;
    
    ortp_set_log_level_mask(NULL, ORTP_FATAL);
    m_pSession = rtp_session_new(RTP_SESSION_SENDONLY);
    rtp_session_set_scheduling_mode(m_pSession, 1);
    rtp_session_set_blocking_mode(m_pSession, 0);
    rtp_session_set_connected_mode(m_pSession, TRUE);
    rtp_session_set_remote_addr(m_pSession, dstIp, dstPort);
    rtp_session_set_payload_type(m_pSession, payload);
    
    m_payload = payload;
    
    ssrc = getenv("SSRC");
    if (ssrc != NULL) {
        printf("using SSRC=%i.\n", atoi(ssrc));
        rtp_session_set_ssrc(m_pSession,atoi(ssrc));
    } else {
        srand((unsigned)time(NULL));
        m_ssrc = rand();
        rtp_session_set_ssrc(m_pSession, m_ssrc);
    }

    return ret;
}

int RtpConn::sendData(uint8_t *data, size_t dataLen)
{
    int ret = 0;
    
    mblk_t *m = rtp_session_create_packet(m_pSession, RTP_FIXED_HEADER_SIZE, data, dataLen);
    rtp_session_sendm_with_ts(m_pSession, m, m_curTimeStamp);

    if (m_payload == G729_PAYLOAD) {
       m_curTimeStamp += dataLen * 8;
//        usleep(dataLen * 1000);
    } else {
        m_curTimeStamp += dataLen / 2;
    }
    
    return ret;
}

int RtpConn::setDataFormat(int mediaType, int mediaFormat)
{
    int ret = 0;
    
//    payload = mediaFormat;
    
    return ret;
}

int RtpConn::release()
{
    rtp_session_destroy(m_pSession);
    
    return 0;
}

int RtpConn::addToSet(SessionSet *set)
{
    m_set = set;
    
    session_set_set(set, m_pSession);
    
    return 0;
}


bool RtpConn::isSet()
{
    if (m_set == NULL || m_pSession == NULL) {
        return false;
    }
    return session_set_is_set(m_set, m_pSession);
}
