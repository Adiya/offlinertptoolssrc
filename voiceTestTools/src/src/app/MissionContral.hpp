//
//  MissionContral.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/29.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef MissionContral_hpp
#define MissionContral_hpp

#include <stdio.h>

#include <map>
#include <string>

class VoiceprintDetectionTask;

class MissionContral {
public:
    MissionContral();
    ~MissionContral();

    bool taskRegister(VoiceprintDetectionTask *task);
    
    bool taskUnregister(VoiceprintDetectionTask *task);
    
    bool hasThisTask(std::string id);
    
    VoiceprintDetectionTask *getTask(std::string id);
    
private:
    std::map<std::string, VoiceprintDetectionTask *> taskMap;   // 任务表
    
    int taskNums;
};

#endif /* MissionContral_hpp */
