//
//  TaskStat.h
//  catchNetPacket
//
//  Created by Adiya on 2018/11/28.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef TaskStat_h
#define TaskStat_h

typedef enum {
    TASK_IDEL       = 0,
    TASK_RUNING     = 1,
    TASK_STOPPING   = 2,
    TASK_STOPED     = 3,
    TASK_UNKNOWN    = 5,
} TaskStat_t;


#endif /* TaskStat_h */
