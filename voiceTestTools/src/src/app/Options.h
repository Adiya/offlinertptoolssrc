//
//  Options.h
//  catchNetPacket
//
//  Created by Adiya on 2018/11/27.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef Options_h
#define Options_h

typedef enum {
    MEM_CACHE_SIZE       = 0,
    DETECT_TIME_BEGIN    = 1,
    DETECT_TIME_DURATION = 2,
    DETECT_TIME_MAX_LIMIT = 3,
} Option_t;

#endif /* Options_h */
