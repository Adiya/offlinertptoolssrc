//
//  MissionContral.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/29.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "MissionContral.hpp"
#include "VoiceprintDetectionTask.hpp"

MissionContral::MissionContral()
{
    taskNums = 0;
}

MissionContral::~MissionContral()
{
    
}

bool MissionContral::taskRegister(VoiceprintDetectionTask *task)
{
    if (task == NULL) {
        return false;
    }
    
    taskNums++;
    
    taskMap.insert(std::pair<std::string, VoiceprintDetectionTask *>(task->getTaskId(), task));
    
    return true;
}

bool MissionContral::taskUnregister(VoiceprintDetectionTask *task)
{
    if (task == NULL) {
        return false;
    }
    
    taskNums--;
    
    std::map<std::string, VoiceprintDetectionTask *>::iterator iter;
    
    iter = taskMap.find(task->getTaskId());
    
    if (iter == taskMap.end()) {
        return false;
    } else {
        taskMap.erase(iter);
    }
    
    return true;
}

bool MissionContral::hasThisTask(std::string id)
{
    
    
    return true;
}


VoiceprintDetectionTask *MissionContral::getTask(std::string id)
{
    std::map<std::string, VoiceprintDetectionTask *>::iterator iter;
    
    iter = taskMap.find(id);
    
    if (iter == taskMap.end()) {
        return NULL;
    }
    
    return iter->second;
}
