//
//  VoiceprintDetectionTask.hpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/27.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#ifndef VoiceprintDetectionTask_hpp
#define VoiceprintDetectionTask_hpp

#include <stdio.h>
#include <pthread.h>

#include <cstdint>
#include <string>
#include <vector>

#include "TaskStat.h"

#include "Options.h"
//
typedef struct ring ring;

class kvpServiceClient;
class ThreadContext;

class VoiceprintDetectionTask {
public:
    VoiceprintDetectionTask();
    ~VoiceprintDetectionTask();
    
    /** 任务参数配置，可选配缓存区大小设置，默认8*1024KB，详细见
     * @ref Option_t
     *
     * @param option 配置类型
     * @param param  配置参数
     * @return true 为正常; false为异常
     */
    bool setParam(Option_t option, uint32_t param);
    
    
    /** 获取对应的配置值
     *
     * @param option 配置类型
     * @return > 0 为正常配置值; 其他为异常
     */
    uint32_t getParam(Option_t option);
    
    /** 配置声纹服务器
     *
     * @param serverAddr 声纹服务器地址
     * @param port       对应应用端口
     * @return true 为正常; false为异常
     */
    bool setServerInfo(std::string serverAddr, int port);
    
    /** 获取声纹服务器域名与端口
     *
     * @param serverAddr 声纹服务器地址
     * @param port       对应应用端口
     * @return true 为正常; false为异常
     */
    bool getServerInfo(std::string &serverAddr, int &port);
    
    /** 启动服务
     *
     * @return true 为正常; false为异常
     */
    bool start();
    
    /** 原始数据传入该任务，等待进行检测任务
     *
     * @param data          数据
     * @param dataLen       数据长度
     * @return == 0 为正常; 其他为异常
     */
    int sendData(uint8_t *data, int dataLen);
    
    /** 处理后的数据
     *
     * @param data          数据
     * @param dataLen       数据长度
     * @return == 0 为正常; 其他为异常
     */
    int recvData(uint8_t *data, int *dataLen);
    
    /** 停止任务，异步调用
     *
     * @return true 为正常; false为异常
     */
    bool stop();
    
    /** 释放内部的数据和缓存，该部分的操作与 sendData 在同一个线程中调用
     *
     */
    void release();
    
    /** 配置任务状态，详细状态值见 @ref TaskStat_t
     *
     * @param stat          状态值
     */
    void setStat(TaskStat_t stat);
    
    /** 获取状态值
     *
     * @return 状态值，详细见  @ref TaskStat_t
     */
    TaskStat_t getStat(void);
    
    /** 配置保存文件
     *
     * @param filename  保存文件名
     */
    void setSaveFilename(std::string filename);
    
    /** 任务等待
     *
     * @return ==0 正常，其他异常
     */
    int wait();
    
    /** 任务等待
     *
     * @param time    超时时间
     * @return ==0 正常，其他异常
     */
    int wait(uint32_t time);
    
    /** 任务通知
     *
     * @return ==0 正常，其他异常
     */
    int signal();
    
    
    int setTaskId(const char *id);
    
    std::string getTaskId();
    
    int getSeqNum();
    
    void setSeqNum(int seq);
    
private:
    
    ThreadContext *detectThread;        // 检测声纹线程
    std::vector<int16_t> pcmBuf;        // pcm数据缓存
    std::string asvServer;    // 声纹服务器地址
    int asvServerPort;        // 对应应用端口
    ring *ringBuf;            // 接收数据的临时环形缓存区
    pthread_mutex_t ringMux;    // 缓存锁
    
    pthread_mutex_t dataMux;
    pthread_cond_t readCond;
    int bufferSize;           // 缓存大小
    
    int detectTimeStart;       // 检测时间起点
    int detectTime;            // 检测时间间隔
    int detectTimeMaxLimit;     // 检测时间间隔
    
    int sampleRate;             // 音频采样
    int members;                // 一对多参数
    long timeout;               // 超时时间，默认3000ms
    
    std::string taskId;         // 任务ID
    
    std::string saveFilename;   // 保存文件名
    
    int rtpSeq;
    
    TaskStat_t taskStat;
    
    int inAudioFormat;
    int outAudioFormat;
    
    std::string srcIp;
    std::string dstIp;
    int srcPort;
    int dstPort;
};

#endif /* VoiceprintDetectionTask_hpp */
