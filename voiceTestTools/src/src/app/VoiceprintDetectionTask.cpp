//
//  VoiceprintDetectionTask.cpp
//  catchNetPacket
//
//  Created by Adiya on 2018/11/27.
//  Copyright © 2018 xqm.co. All rights reserved.
//


#include <iostream>

#include "VoiceprintDetectionTask.hpp"

#include "CnUtils.h"

#include "thrift/transport/TSocket.h"
#include "thrift/transport/TBufferTransports.h"
#include "thrift/protocol/TBinaryProtocol.h"

#include "asv/kvpService.h"
#include "RingBuffer.hpp"
#include "ThreadContext.hpp"

#include <boost/shared_ptr.hpp>

#include "g729a.h"

using boost::shared_ptr;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

//static const char *geLocalTime()
//{
//    time_t             timesec;
//    struct tm         *p;
//
//
//    time(×ec);
//    p = localtime(×ec);
//
//    printf("%d%d%d%d%d%d\n", 1900+p->tm_year, 1+p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec);
//
//}

//static const int decode_len = 10;
static void *voiceDectectThread(void *arg)
{
    if (arg == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Voice Dectect Thread create error, parameter invaild\n");
        return NULL;
    }
    VoiceprintDetectionTask *task = (VoiceprintDetectionTask *)arg;
    uint32_t memCacheSize;
    uint32_t detectTimeBegin;
    uint32_t detectTimeDuration;
    uint32_t detectTimeMaxLimit;
    std::vector<std::string> nodes;
    std::vector<int16_t> pcmBuf;
    uint8_t *pPcmDataData;
    int16_t pcmShortDataBuf[L_FRAME];
    int pcmDataLen = L_FRAME * sizeof(int16_t);
    Rpc_TopSpeakerInfo tinfo;
    int ret;
    std::string addr;
    long baseSamples;
    long maxSamples;
    long deltaSamples;
    int members = 2;
    int timeout = 100;
    int port;
    FILE *fp;
    char filename[2048];
    
    memCacheSize = task->getParam(MEM_CACHE_SIZE);
    detectTimeBegin = task->getParam(DETECT_TIME_BEGIN);
    detectTimeDuration = task->getParam(DETECT_TIME_DURATION);
    detectTimeMaxLimit = task->getParam(DETECT_TIME_MAX_LIMIT);
    if (memCacheSize <= 0 || detectTimeBegin <= 0 || detectTimeDuration <= 0 || detectTimeMaxLimit <= 0) {
        av_log(NULL, AV_LOG_ERROR, "Parameter is invaild![%u][%u][%u][%u]\n", memCacheSize, detectTimeBegin, detectTimeDuration, detectTimeMaxLimit);
        return NULL;
    }
    
    task->getServerInfo(addr, port);
    
    boost::shared_ptr<TSocket> socket_asv(new TSocket(addr, port));
    boost::shared_ptr<TTransport> transport_asv(new TFramedTransport(socket_asv));
    boost::shared_ptr<TProtocol> protocol_asv(new TBinaryProtocol(transport_asv));
    
    kvpServiceClient asvClient(protocol_asv);
    
    try {
        transport_asv->open();
        
        char node[64] = {"cmb1025"};
        
        fprintf(stderr, "Insert node ...\n");
        asvClient.kvpInsertNode(node);
        fprintf(stderr, "Insert node complete!!!\n");
        
        nodes.push_back(node);
        
    }  catch (TException& tx) {
        std::cout << "ERROR: " << tx.what() << std::endl;
    }
    
    pPcmDataData = (uint8_t *)pcmShortDataBuf;
    baseSamples = 8000 * 15;
    maxSamples = 8000 * 60;
    deltaSamples = 8000 * 3;
    
    time_t timep;
    struct tm *p;
    time(&timep);
    printf("time() : %ld \n", timep);
    p=localtime(&timep);
    timep = mktime(p);
    
    snprintf(filename, sizeof(filename), "%p_%ld_%s_8000_s16_1.pcm", task, timep, task->getTaskId().c_str());
    
    fp = fopen(filename, "w+");
    if (fp == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Open Save file %s error!\n", task->getTaskId().c_str());
        return NULL;
    }
    
    uint32_t totalTimeout = 0;
    while (true) {
        if (task->getStat() > TASK_RUNING) {
            av_log(NULL, AV_LOG_INFO, "[%p]Task exit[%d]\n", task, task->getStat());
            break;
        }
        ret = task->recvData(pPcmDataData, &pcmDataLen);
        if (ret < 0) {
            // TODO: wait
            task->wait(timeout);
            totalTimeout++;
            if (totalTimeout > 30) {
                task->setStat(TASK_STOPPING);
                av_log(NULL, AV_LOG_WARNING, "[%p]Receive data timeout! exit thread\n", task);
                break;
            } else {
                continue;
            }
        }
        
//        readDataLen += pcmDataLen; //debug

        if (fp) {
            ret = fwrite(pPcmDataData, 1, pcmDataLen, fp);
        }
        
        pcmBuf.reserve(pcmBuf.size() + L_FRAME);
        pcmBuf.insert(pcmBuf.end(), &pcmShortDataBuf[0], &pcmShortDataBuf[L_FRAME]);
        // reset
        totalTimeout = 0;
        
        if (pcmBuf.size() >= baseSamples) {
            if ((pcmBuf.size() - baseSamples) % deltaSamples == 0) {
                try {
                    asvClient.kvpIdentifyTopSpeakerByStream(tinfo, pcmBuf, 8000, nodes, 1, members, 0);
                    if (tinfo.ErrCode == 0) {
                        if (tinfo.Scores.size() > 0) {
                            fprintf(stderr, "[%p]impostor: %d | %s: %f\n", task, tinfo.ErrCode,  tinfo.Scores[0].Spkid.c_str(), tinfo.Scores[0].Score);
                        } else {
                            av_log(NULL, AV_LOG_WARNING, "Not found audio scores\n");
                        }
                    } else {
                        fprintf(stderr,"impostor: error: %d\n", tinfo.ErrCode);
                    }
                } catch (TException& tx) {
                    std::cout << "ERROR: " << tx.what() << std::endl;
                }
                
                std::vector <int16_t>::iterator Iter;
                if (pcmBuf.size() >= maxSamples) {

                    
#if 0
                    struct timeval begin, end;
                    gettimeofday(&begin, NULL);
                    av_log(NULL, AV_LOG_INFO, "0pcmBuf size %ld\n", pcmBuf.size());
#endif
                    Iter = pcmBuf.begin();
                    pcmBuf.erase(Iter, Iter+deltaSamples);
                    
#if 0
                    gettimeofday(&end, NULL);
                    av_log(NULL, AV_LOG_INFO, "1pcmBuf size %ld use time %ld\n", pcmBuf.size(), (end.tv_sec - begin.tv_sec) * 1000000 + end.tv_usec - begin.tv_usec);
#endif
#if 0
                    i = 0;
                    for(Iter = pcmBuf.begin(); Iter != pcmBuf.end(); Iter++) {
                        if (pcmBuf.size() == 0) {
                            break;
                        }
                        if (task->getStat() > TASK_RUNING) {
                            printf("[%p]Thread need exit!\n", task);
                            goto TAR_OUT;
                        }
                        Iter = pcmBuf.erase(Iter);
                        Iter--;
                        i++;
                        if (i >= deltaSamples) {
                            break;
                        }
                    }
#endif
                }
            }
        }
    }
    
TAR_OUT:
    
    if (pcmBuf.size()) {
        pcmBuf.clear();
        std::vector<int16_t>().swap(pcmBuf);
    }
    
    if (fp) {
        fclose(fp);
    }
    
    transport_asv->close();
    
    av_log(NULL, AV_LOG_INFO, "Voiceprint Detect thread exit!\n");
    
    return NULL;
}

VoiceprintDetectionTask::VoiceprintDetectionTask():
bufferSize(1024 * 1024),
detectTimeStart(15),
detectTime(3),
sampleRate(8000),
detectTimeMaxLimit(60)
{
    members         = 2;
    taskStat        = TASK_IDEL;
    detectThread    = NULL;
    timeout         = 3000;
    rtpSeq          = 0;
}

VoiceprintDetectionTask::~VoiceprintDetectionTask()
{
    
}

bool VoiceprintDetectionTask::setParam(Option_t option, uint32_t param)
{
    switch (option) {
        case MEM_CACHE_SIZE:
            bufferSize = param;
            break;
            
        case DETECT_TIME_DURATION:
            detectTime = param;
            break;
            
        default:
            return false;
    }
    
    return true;
}

uint32_t VoiceprintDetectionTask::getParam(Option_t option)
{
    uint32_t ret = 0;
    
    switch (option) {
        case MEM_CACHE_SIZE:
            ret = bufferSize;
            break;
            
        case DETECT_TIME_BEGIN:
            ret = detectTimeStart;
            break;
            
        case DETECT_TIME_MAX_LIMIT:
            ret = detectTimeMaxLimit;
            break;
            
        case DETECT_TIME_DURATION:
            ret = detectTime;
            break;
            
        default:
            break;
    }
    
    return ret;
}

bool VoiceprintDetectionTask::setServerInfo(std::string addr, int port)
{
    if ((addr.length() == 0) || port <= 0) {
        return false;
    }
    
    asvServer = addr;
    asvServerPort = port;
    
    return true;
}

TaskStat_t VoiceprintDetectionTask::getStat(void)
{
    return taskStat;
}

void VoiceprintDetectionTask::setStat(TaskStat_t stat)
{
    taskStat = stat;
}

bool VoiceprintDetectionTask::getServerInfo(std::string &addr, int &port)
{
    addr = asvServer;
    port = asvServerPort;
    
    return true;
}

bool VoiceprintDetectionTask::start()
{
    int ret;
    
    if (detectThread) {
        av_log(NULL, AV_LOG_WARNING, "Audio detect thread already exist![%s]\n", detectThread->getThreadName());
        return false;
    }
    
    if (bufferSize <= 0) {
        return false;
    }
    
    pthread_mutex_init(&ringMux, NULL);
    
    ringBuf = ring_alloc(bufferSize, &ringMux);
    if (ringBuf == NULL) {
        return false;
    }
    
    detectThread = new ThreadContext();
    if (detectThread == NULL) {
        av_log(NULL, AV_LOG_ERROR, "Alloc detect thread error!\n");
        return false;
    }
    
    ret = detectThread->setFunction(voiceDectectThread, this);
    if (ret < 0) {
        return false;
    }
    detectThread->start();
    
    pthread_cond_init(&readCond, NULL);
    pthread_mutex_init(&dataMux, NULL);
    
    taskStat = TASK_RUNING;
    
    return true;
}

int VoiceprintDetectionTask::sendData(uint8_t *data, int dataLen)
{
    uint32_t len;
    
    if (ringBuf == NULL || taskStat != TASK_RUNING) {
        return AVERROR(EINVAL);
    }
    
    len = ring_len(ringBuf);
    if ((bufferSize - len) < dataLen) {
        av_log(NULL, AV_LOG_WARNING, "Ring buffer is full[%d<%d]\n", len, dataLen);
        return AVERROR(EACCES);
    }
    
    len = ring_put(ringBuf, data, dataLen);
    if (len == 0) {
        av_log(NULL, AV_LOG_WARNING, "Put audio data to ring buffer len is 0, please check audio in parameter\n");
    }
    
    this->signal();
    
    return 0;
}

int VoiceprintDetectionTask::recvData(uint8_t *data, int *dataLen)
{
    int ringLen = ring_len(ringBuf);
    int len;
    uint8_t pG729SampleData[L_FRAME_COMPRESSED];
    
    if (ringLen >= *dataLen) {
        len = ring_get(ringBuf, pG729SampleData, L_FRAME_COMPRESSED);
        if (len <= 0) {
            av_log(NULL, AV_LOG_ERROR, "Get buffer error!\n");
            return AVERROR(ENOMEM);
        }
        va_g729a_decoder((unsigned char *)pG729SampleData, (short *)data, 0);
    } else {
        return -1;
    }
    
    return 0;
}

bool VoiceprintDetectionTask::stop()
{
    taskStat = TASK_STOPPING;
    
    if (detectThread) {
        signal();
        detectThread->join();
        delete detectThread;
        detectThread = NULL;
    }
    
    ring_free(ringBuf);
    ringBuf = NULL;
    taskStat = TASK_STOPED;
    
    return true;
}

int VoiceprintDetectionTask::wait()
{
    pthread_mutex_lock(&dataMux);
    pthread_cond_wait(&readCond, &dataMux);
    pthread_mutex_unlock(&dataMux);
    
    return 0;
}

int VoiceprintDetectionTask::wait(uint32_t ms)
{
    int ret;
    int retval;
    struct timeval delta;
    struct timespec abstime;
    
    gettimeofday(&delta, NULL);
    abstime.tv_sec = delta.tv_sec + (ms / 1000);
    abstime.tv_nsec = (delta.tv_usec + (ms % 1000) * 1000) * 1000;
    if (abstime.tv_nsec > 1000000000) {
        abstime.tv_sec += 1;
        abstime.tv_nsec -= 1000000000;
    }
    
    pthread_mutex_lock(&dataMux);
    while (1) {
        retval = pthread_cond_timedwait(&readCond, &dataMux, &abstime);
        if (retval == 0) {
            ret = 0;
            break;
        } else if (retval == EINTR) {
            continue;
        } else if (retval == ETIMEDOUT) {
            ret = 1;
            break;
        } else {
            ret = -1;
            break;
        }
    }
    pthread_mutex_unlock(&dataMux);
    
    return ret;
}

int VoiceprintDetectionTask::signal()
{
    pthread_mutex_lock(&dataMux);
    pthread_cond_signal(&readCond);
    pthread_mutex_unlock(&dataMux);
    
    return 0;
}

int VoiceprintDetectionTask::setTaskId(const char *id)
{
    if (id == NULL) {
        return -1;
    }
    
    taskId = id;
    
    return 0;
}

std::string VoiceprintDetectionTask::getTaskId()
{
    return taskId;
}

int VoiceprintDetectionTask::getSeqNum()
{
    return rtpSeq;
}

void VoiceprintDetectionTask::setSeqNum(int seq)
{
    rtpSeq = seq;
}
