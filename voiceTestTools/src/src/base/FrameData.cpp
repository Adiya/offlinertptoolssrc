//
//  FrameData.cpp
//  readFrame
//
//  Created by xqm on 2018/3/9.
//  Copyright © 2018年 xqm. All rights reserved.
//

#include "FrameData.hpp"
#include "MediaFormat.h"
#include "MemUtils.h"
#include "InnerFormatTransfer.h"
#include "FFmpegUtils.h"
#include "MTError.h"

#include <sys/time.h>
#include <string.h>
#include <stdlib.h>

namespace MMTOOLS {
    
#define DEBUG_OUT_YUV_FILE 0
    
FrameData::FrameData()
{
    mediaFrameType = MT_MEDIA_TYPE_NONE;
    inFormat = MT_SAMPLE_FMT_NONE;
    outFormat = MT_SAMPLE_FMT_NONE;
    outAudioFormat = NULL;
    inAudioFormat = NULL;
    outVideoFormat = NULL;
    dataSize = 0;
    data = NULL;
    streamIndex = -1;
    newData = NULL;
    newDataSize = 0;
    inDataPerSampleSize = -1;
    outDataPerSampleSize = -1;
    isKeyFrame = false;
}

FrameData::~FrameData()
{
    printf("Release frame data\n");
    if (inAudioFormat) {
        SAFE_FREE(inAudioFormat);
    }
    if (outAudioFormat) {
        SAFE_FREE(outAudioFormat);
    }
    if (outVideoFormat) {
        SAFE_FREE(outVideoFormat);
    }
    if (data) {
        av_freep(&data);
    }
    
    if (newData) {
        av_freep(&newData);
    }
    
#if DEBUG_OUT_YUV_FILE
    if (yuvfp) {
        fclose(yuvfp);
    }
#endif
}
    
int  FrameData::setInAudioDataFormat(const AudioParam_t *audioFormat)
{
    int ret = AV_NOT_INIT;
    
    if (inAudioFormat == NULL) {
        inAudioFormat = (AudioParam_t *)av_mallocz(sizeof(AudioParam_t));
        if (inAudioFormat == NULL) {
            return AV_MALLOC_ERR;
        }
    }

    if (audioFormat->channels == inAudioFormat->channels &&
         audioFormat->sampleFmt == inAudioFormat->sampleFmt &&
        audioFormat->sampleRate == inAudioFormat->sampleRate) {
        av_log(NULL, AV_LOG_DEBUG, "Already init resampler\n");
        return 0;
    }
    
    if (inAudioFormat && (inAudioFormat->channels > 0 && \
                          inAudioFormat->sampleFmt != MT_SAMPLE_FMT_NONE && \
                          inAudioFormat->sampleRate > 0) && \
                        (audioFormat->channels != inAudioFormat->channels ||
                        audioFormat->sampleFmt != inAudioFormat->sampleFmt ||
                         audioFormat->sampleRate != inAudioFormat->sampleRate)) {
                            av_log(NULL, AV_LOG_INFO, "Need resample\n");

    }
    
    memcpy(inAudioFormat, audioFormat, sizeof(AudioParam_t));
    
    if (outAudioFormat &&  (outAudioFormat->channels != inAudioFormat->channels || \
                           outAudioFormat->sampleFmt != inAudioFormat->sampleFmt || \
                           outAudioFormat->sampleRate != inAudioFormat->sampleRate)) {
    }
    
    return ret;
}
    
int FrameData::setInMediaDataFormat(const enum MediaType_t type, const int format)
{
    if (type <= MT_MEDIA_TYPE_NONE) {
        return -1;
    }
    mediaFrameType = type;
    inFormat = format;
    
    if (mediaFrameType == MT_MEDIA_TYPE_AUDIO) {
        if (inAudioFormat == NULL) {
            inAudioFormat = (AudioParam_t *)malloc(sizeof(AudioParam_t));
            if (inAudioFormat == NULL) {
                return AV_MALLOC_ERR;
            }
        }

        inDataPerSampleSize = av_get_bytes_per_sample((AVSampleFormat)inFormat);
        inAudioFormat->sampleFmt = (enum AUDIO_SAMPLE_FORMAT)format;
        
    } else if (mediaFrameType == MT_MEDIA_TYPE_VIDEO) {
        
    } else {
        
    }
    
    return 0;
}
    
int FrameData::setInMediaDataFormat(const enum MediaType_t type, const int format, int streamIndex)
{
    int ret;
    
    ret = setInMediaDataFormat(type, format);
    if (ret < 0) {
        return ret;
    }
    
    this->streamIndex = streamIndex;
    
    return ret;
}

int FrameData::setOutAudioDataFormat(const AudioParam_t *outFormat)
{
    if (outFormat == NULL) {
        return AV_PARM_ERR;
    }
    
    if (outAudioFormat == NULL) {
        outAudioFormat = (AudioParam_t *)av_mallocz(sizeof(AudioParam_t));
        if (outAudioFormat == NULL) {
            return AV_MALLOC_ERR;
        }
    }

    
    if (outAudioFormat->channels == outFormat->channels && \
        outAudioFormat->sampleFmt == outFormat->sampleFmt && \
        outAudioFormat->sampleRate == outFormat->sampleRate) {
        return 0;
    }
    
    outDataPerSampleSize = av_get_bytes_per_sample(getAudioInnerFormat(outFormat->sampleFmt));
    if (outDataPerSampleSize < 0) {
        av_log(NULL, AV_LOG_WARNING, "Out Data format is not support!\n");
    }
    
    // 如果事先已经有resample，先重置
    if (outAudioFormat->channels > 0 && outAudioFormat->sampleFmt != MT_SAMPLE_FMT_NONE && \
        outAudioFormat->sampleRate >0 && \
        (outAudioFormat->channels != outFormat->channels ||
        outAudioFormat->sampleFmt != outFormat->sampleFmt ||
        outAudioFormat->channelLayout != outFormat->channelLayout ||
        outAudioFormat->sampleRate != outFormat->sampleRate)) {

    }
    
    outAudioFormat->channels = outFormat->channels;
    outAudioFormat->sampleFmt = outFormat->sampleFmt;
    outAudioFormat->sampleRate = outFormat->sampleRate;
    outAudioFormat->channelLayout = outFormat->channelLayout;
    outAudioFormat->sampleSize = outFormat->sampleSize;
    
    if (inAudioFormat && (outAudioFormat->channels != inAudioFormat->channels || \
        outAudioFormat->sampleFmt != inAudioFormat->sampleFmt || \
        outAudioFormat->sampleRate != inAudioFormat->sampleRate)) {

    }
    
    return 0;
}

AudioParam_t *FrameData::getOutAudioDataFormat(void)
{
    return outAudioFormat;
}
    
AudioParam_t *FrameData::getInAudioDataInfo(void)
{
    return inAudioFormat;
}


int FrameData::getInMediaDataFormat(enum MediaType_t &type, int &format)
{
    if (mediaFrameType <= MT_MEDIA_TYPE_NONE) {
        return -1;
    }
    
    type = (enum MediaType_t)mediaFrameType;
    format = inFormat;
    
    return 0;
}

int FrameData::write(uint8_t *indata, size_t dataSize)
{
    if (indata == NULL || dataSize <= 0) {
        return AV_PARM_ERR;
    }
    
    if (dataSize > this->dataSize) {
        if (data) {
            free(data);
        }
        data = (uint8_t *)malloc(dataSize);
        if (data == NULL) {
            return AV_MALLOC_ERR;
        }
    }
    
    memmove(data, indata, dataSize);
    
    this->dataSize = dataSize;
    
    return 0;
}
    
int FrameData::write(uint8_t *indata[], int samples)
{// 该函数暂时未正式启用，dummy code
    if (indata == NULL || samples <= 0) {
        return AV_PARM_ERR;
    }

    memmove(data, indata, dataSize);
    
    this->dataSize = dataSize;
    
    return 0;
}
    
int64_t FrameData::getMediaPts(void)
{
    return pts;
}

    
int FrameData::transfer()
{
    int ret = -1;
    
    if (mediaFrameType == MT_MEDIA_TYPE_AUDIO) {
        
    } else if (mediaFrameType == MT_MEDIA_TYPE_VIDEO) {
        av_log(NULL, AV_LOG_INFO, "video\n");
    } else {
        av_log(NULL, AV_LOG_INFO, "This format cannot support %d\n", type);
        
    }
    
TAR_OUT:
    
    return ret;
}

uint8_t *FrameData::read(size_t &size)
{
    if (mediaFrameType == MT_MEDIA_TYPE_AUDIO) {
        size = this->dataSize;
        return data;
    } else if (mediaFrameType == MT_MEDIA_TYPE_VIDEO) {
        return NULL;
    } else {
        return NULL;
    }
}
    
void FrameData::release()
{
    if (inAudioFormat) {
        av_freep(&inAudioFormat);
    }
    if (outAudioFormat) {
        av_freep(&outAudioFormat);
    }
    
    if (data) {
        av_freep(&data);
        dataSize = 0;
    }
    if (newData) {
        av_freep(&newData);
        newDataSize = 0;
    }
}
    
long FrameData::getOutMediaSize(void)
{
    return 0;
}
    
int FrameData::setOutMediaSize(long outSize)
{
    if (outSize > newDataSize) {
        if (newData) {
            av_freep(&newData);
        }
        
        dataSize = outSize;
        newDataSize = outSize;
        newData = (uint8_t *)av_malloc(outSize);
        if (newData == NULL) {
            av_log(NULL, AV_LOG_ERROR, "Alloc new data error!\n");
            return  AV_MALLOC_ERR;
        }
    }
    
    return 0;
}
}
