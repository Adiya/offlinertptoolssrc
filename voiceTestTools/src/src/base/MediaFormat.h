//
//  MediaFormat.h
//  MediaRecordDemo
//
//  Created by xqm on 2017/8/15.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef MediaFormat_h
#define MediaFormat_h

#include "MediaBaseType.h"

#include <stdint.h>

typedef enum VIDEO_ROTATE_t {
    MT_Rotate0      = 0,  // No rotation.
    MT_Rotate90     = 90,  // Rotate 90 degrees clockwise.
    MT_Rotate180    = 180,  // Rotate 180 degrees.
    MT_Rotate270    = 270,  // Rotate 270 degrees clockwise.
} videoRorate_t;

// 视频裁剪参数
typedef struct cropParam_t {
    int posX;
    int posY;
    int cropWidth;
    int cropHeight;
    int lineSize[4];
} cropParam_t;

// 视频裁剪参数
typedef struct ScaleParam_t {
    int inWidth;
    int inHeight;
    int outWidth;
    int outHeight;
    int scaleMode;   // 0~3越大，速度越慢，质量越高
    int lineSize[4];
} ScaleParam_t;

enum AUDIO_SAMPLE_FORMAT {
    MT_SAMPLE_FMT_NONE = 0,
    MT_SAMPLE_FMT_U8,          ///< unsigned 8 bits
    MT_SAMPLE_FMT_S16,         ///< signed 16 bits
    MT_SAMPLE_FMT_S32,         ///< signed 32 bits
    MT_SAMPLE_FMT_FLT,         ///< float
    MT_SAMPLE_FMT_DBL,         ///< double
    
    MT_SAMPLE_FMT_U8P,         ///< unsigned 8 bits, planar
    MT_SAMPLE_FMT_S16P,        ///< signed 16 bits, planar
    MT_SAMPLE_FMT_S32P,        ///< signed 32 bits, planar
    MT_SAMPLE_FMT_FLTP,        ///< float, planar
    
    MT_SAMPLE_FMT_NB           ///< Number of sample formats. DO NOT USE if linking dynamically
};

enum VIDEO_PIX_FORMAT {
    MT_PIX_FMT_NONE     = -1,
    MT_PIX_FMT_YUV420P  = 0,
    MT_PIX_FMT_YUV422P  = 1,
    MT_PIX_FMT_YUV444P  = 2,
    MT_PIX_FMT_NV12     = 3,
    MT_PIX_FMT_NV21     = 4,
    MT_PIX_FMT_ARGB     = 5,
    MT_PIX_FMT_RGBA     = 6,
    MT_PIX_FMT_ABGR     = 7,
    MT_PIX_FMT_BGRA     = 8,
};

typedef struct AudioParam_t {
    int64_t channelLayout;              // 音频数据排布方式
    int channels;                       // 音频通道数
    int sampleRate;                     // 音频采样率 Hz
    enum AUDIO_SAMPLE_FORMAT sampleFmt; // 音频采样格式
    int bitrate;                        // 音频码率
    bool isPlannar;                     // 判断是否为平面存放
    int sampleSize;                     // 音频数据量
} AudioParam_t;

typedef struct VideoParam_t {
    int keyFrameInterval;               // 关键帧间隔
    int videoWidth;                     // 视频宽
    int videoHeight;                    // 视频高
    enum VIDEO_PIX_FORMAT videoFmt;     // 视频数据输入格式
    int bitrate;                        // 视频码率 单位kb/s
    int videoPixBufSize;                // 视频单帧缓存大小
    int rotate;                         // 反转角度
    float videoCrf;                     // 视频crf参数
    int lineSize[4];                    // 视频各个分量的值
} VideoParam_t;

typedef struct StreamParam_t {
    int streamIndex;
    int mediaType;                          // 媒体信息 详细类型见enum MT_MEDIA_TYPE_*
    int64_t bitrate;                        // 视频码率 单位 bps
    char codecName[128];                    // 编码器名称
    int64_t streamDuration;             // 流时长，单位us
    
    int64_t frameCnt;                   // 帧数目
    /*  视频专有*/
    int videoWidth;                     // 视频宽
    int videoHeight;                    // 视频高
    enum VIDEO_PIX_FORMAT videoFmt;     // 视频数据输入格式
    float frameRate;                    // 视频帧率
    int videoPixBufSize;                // 视频单帧缓存大小
    int rotate;                         // 反转角度
    float videoCrf;                     // 视频crf参数
    int lineSize[4];                    // 视频各个分量的值
    MTRational_t sar;                   // 视频像素宽高比
    /* 音频专有 */
    int channels;                       // 音频通道数
    int sampleRate;                     // 音频采样率 Hz
    int nbSamples;                      // 单个帧解码出来的数据量（单位sample）
    enum AUDIO_SAMPLE_FORMAT sampleFmt; // 音频采样格式
} StreamParam_t;

typedef struct MediaParam_t {
    StreamParam_t *streamParam[MAX_STREAM_NUM];
    int64_t duration;                         // 文件时长，单位us
    int nbStreams;
} MediaParam_t;

#endif /* MediaFormat_h */
