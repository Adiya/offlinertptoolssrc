//
//  Version.h
//  MediaRecordDemo
//
//  Created by xqm on 2017/9/6.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef Version_h
#define Version_h

namespace MTTranscode {

static const char *version = "0.0.0.1";

static const char *corporation = "kuaishangtong.co";

static const char *author = "xqm";

static const char *getVersion()
{
    return version;
}

static const char *getCorporation()
{
    return corporation;
}

static const char *getAuthor()
{
    return author;
}
}
#endif /* Version_h */
