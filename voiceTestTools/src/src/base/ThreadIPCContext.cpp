//
//  ThreadIPCContext.cpp
//  MediaRecordDemo
//
//  Created by meitu on 2017/8/18.
//  Copyright © 2017年 meitu. All rights reserved.
//

#include <sys/time.h>
#include <errno.h>

#include "MTError.h"
#include "ThreadIPCContext.hpp"

namespace MMTOOLS {
    
ThreadIPCContext::ThreadIPCContext(int initNUm)
{
    condCnt = initNUm;
    
    pthread_cond_init(&cond, NULL);
    pthread_mutex_init(&condMux, NULL);
}

ThreadIPCContext::~ThreadIPCContext()
{

}

int ThreadIPCContext::condV()
{
    pthread_mutex_lock(&condMux);
    pthread_cond_signal(&cond);
    condCnt++;
    pthread_mutex_unlock(&condMux);
    
    return 0;
}

int ThreadIPCContext::condP()
{
    pthread_mutex_lock(&condMux);
    if (condCnt <= 0) {
        pthread_cond_wait(&cond, &condMux);
    }
    condCnt--;
    pthread_mutex_unlock(&condMux);
    
    return 0;
}
    
int ThreadIPCContext::condP(int64_t waitTime)
{
    int retval;
    struct timeval delta;
    struct timespec abstime;
    
    gettimeofday(&delta, NULL);
    
    abstime.tv_sec = delta.tv_sec + (waitTime / 1000000);
    abstime.tv_nsec = (delta.tv_usec + (waitTime % 1000000) * 1000000);
    if (abstime.tv_nsec > 1000000000) {
        abstime.tv_sec += 1;
        abstime.tv_nsec -= 1000000000;
    }
    
    while (1) {
        retval = pthread_cond_timedwait(&cond, &condMux, &abstime);
        if (retval == 0)
            return 0;
        else if (retval == EINTR)
            continue;
        else if (retval == ETIMEDOUT)
            return AV_TIMEOUT;
        else
            break;
    }
    
    return -1;
}
    
int ThreadIPCContext::reset()
{
    if (condCnt > 0) {
        condCnt = 0;
        return 0;
    } else {
        return AV_SET_ERR;
    }
}
    
int ThreadIPCContext::release()
{
    pthread_cond_destroy(&cond);
    pthread_mutex_destroy(&condMux);
    
    condCnt = -1;
    
    return 0;
}
}
