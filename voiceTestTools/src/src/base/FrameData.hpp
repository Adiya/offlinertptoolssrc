//
//  FrameData.hpp
//  readFrame
//
//  Created by xqm on 2018/3/9.
//  Copyright © 2018年 xqm. All rights reserved.
//

#ifndef FrameData_hpp
#define FrameData_hpp

#include <stdio.h>
#include <inttypes.h>

#include "MediaFormat.h"

namespace MMTOOLS {

class FrameData {
public:
    FrameData();
    ~FrameData();
    
    /*
     * @func 设置输入的音频格式数据.
     * @param inAudioFormat 音频格式数据
     * @return 0 为正常; < 0为异常
     */
    int setInAudioDataFormat(const AudioParam_t *inAudioFormat);
    
    /*
     * @func 设置输出的音频格式数据.
     * @param outAudioFormat 音频格式数据
     * @return 0 为正常; < 0为异常
     */
    int setOutAudioDataFormat(const AudioParam_t *outAudioFormat);

    /*
     * @func 获取输出的音频格式数据.
     * @return NULL为异常，其他为音频配置信息结构体
     */
    AudioParam_t *getOutAudioDataFormat(void);
    
    /*
     * @func 获取输入的音频格式数据.
     * @return NULL为异常，其他为音频配置信息结构体
     */
    AudioParam_t *getInAudioDataInfo(void);
    
    /*
     * @func 设置输入多媒体数据信息.
     * @param type 多媒体类型
     * @param format 输入格式
     * @return 0为正常，其他为异常
     */
    int setInMediaDataFormat(const enum MediaType_t type, const int format);
    
    /*
     * @func 设置输入多媒体数据信息.
     * @param type 多媒体类型
     * @param format 输入格式
     * @param streamIndex 流标示
     * @return 0为正常，其他为异常
     */
    int setInMediaDataFormat(const enum MediaType_t type, const int format, int streamIndex);

    /*
     * @func 获取输入多媒体信息.
     * @param type 多媒体类型引用
     * @param format 输入格式引用
     * @return 音频为AUDIO_SAMPLE_FORMAT enum类型 视频为VIDEO_PIX_FORMAT enum类型
     */
    int getInMediaDataFormat(enum MediaType_t &type, int &format);
    
    /*
     * @func 数据转换.
     * @param filename 文件名
     * @return 0 为正常; < 0为异常
     */
    int transfer();
    
    /*
     * @func 写入媒体数据.
     * @param data 数据指针
     * @param dataSize 数据大小
     * @return = 0 为正常，其他为异常。
     */
    int write(uint8_t *data, size_t dataSize);
    
    /*
     * @func 写入媒体数据.
     * @param indata 数据指针数组
     * @param samples 数据点数
     * @return = 0 为正常，其他为异常。
     */
    // TODO 该部分还未正常工作，该部分是为了后面方便多个音轨数据的处理
    int write(uint8_t *indata[], int samples);
    
    /*
     * @func 读取数据.
     * @param size 数据大小
     * @return !=NULL 为正常; 其余为异常
     */
    uint8_t *read(size_t &size);
    
    /*
     * @func 释放数据
     * @param void
     * @return void
     */
    void release(void);
    
    /*
     * @func 读取该帧的时间戳，单位us.
     * @param void
     * @return 媒体时间戳
     */
    int64_t getMediaPts(void);
    
    /*
     * @func 当配置输出的格式后，对输出数据量进行预估
     * @param void
     * @return > 0 正常，其他为异常
     */
    long getOutMediaSize(void);
    
    /*
     * @func 该接口主要是用于对输出的数据进行加减速处理，主要用于音频数据的加减速
     * @param outSize 输出数据大小
     * @return == 0 正常，其他为异常
     */
    int setOutMediaSize(long outSize);
    
public:
    // 输入音频格式
    AudioParam_t *inAudioFormat;
    
    
    // 数据的pts
    int64_t pts;
    
    // 是否为关键帧标记，该部分仅对视频有效
    bool isKeyFrame;
    
private:
    // 多媒体数据
    uint8_t *data;
    
    // 数据大小
    size_t dataSize;
    
    // 转换后多媒体数据
    uint8_t *newData;
    
    // 开辟的多媒体数据大小
    size_t newDataSize;
    
    // 转换后数据量大小
    size_t outDataSize;
    
    // 流标示
    int streamIndex;
    
    // 媒体格式
    enum MediaType_t type;
    
    // 对齐方式
    int align;
    
    // 输出音频格式
    AudioParam_t *outAudioFormat;
    
    // 输出视频格式
    VideoParam_t *outVideoFormat;
    // 媒体格式
    enum MediaType_t mediaFrameType;
    
    // 输入格式
    int inFormat;
    // 输出格式
    int outFormat;
    
    // 输入数据，单个点占用数据大小
    int inDataPerSampleSize;
    
    // 输出数据，单个点占用数据大小
    int outDataPerSampleSize;
};

}

#endif /* FrameData_hpp */
