//
//  ThreadContext.hpp
//  MediaRecordDemo
//
//  Created by xqm on 2017/8/18.
//  Copyright © 2017年 xqm. All rights reserved.
//

#ifndef ThreadContext_hpp
#define ThreadContext_hpp

#include "ThreadIPCContext.hpp"
#include <stdio.h>

typedef void *(callback)(void *handle, int stat);
typedef void *(*func)(void *arg);
typedef struct ThreadParam_t ThreadParam_t;

enum ThreadStat_t {
    THREAD_IDLE         = 0,
    THREAD_PREPARE      = 1,
    THREAD_RUNNING      = 2,
    THREAD_STOP         = 3,
    THREAD_STOPPING     = 4,
    THREAD_DEAD         = 5,
    THREAD_FORCEQUIT    = 6,
};

namespace MMTOOLS {

/*
 * @class 线程控制类，用于创建线程
 */
class ThreadContext {
public:
    ThreadContext();
    ~ThreadContext();
    
    /*
     * @func 设置线程参数接口
     * @arg function 线程处理函数指针
     * @arg arg 线程参数
     * @arg threadName 线程名称
     * @return 0 为正常, 其他为异常
     */
    int setFunction(func function, void *arg, const char *threadName="MTThread");
    
    /*
     * @func 获取线程状态
     * @return ThreadStat_t 对应枚举
     */
    int getThreadState();
    
    /*
     * @func 启动线程
     * @return 0正常，其他异常
     */
    int start();
    
    /*
     * @func 终端线程（常规情况不推荐使用）
     * @return 0正常，其他异常
     */
    int abort();
    
    /*
     * @func 等待线程终止
     * @return 0正常，其他异常
     */
    int join();
    
    /*
     * @func 分离线程
     * @return 0正常，其他异常
     */
    int detach();
    
    /*
     * @func 标记线程结束
     * @return 0正常，其他异常
     */
    int markOver();

private:
    ThreadParam_t *threadArg;
};
}
#endif /* ThreadContext_hpp */
