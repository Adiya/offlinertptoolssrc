//
//  Error.h
//  admanagerment
//
//  Created by xqm on 29/04/2017.
//  Copyright © 2017 xqm. All rights reserved.
//

#ifndef MTError_h
#define MTError_h

namespace MMTOOLS {

typedef enum ErrorCode {
    AV_STAT_ERR     = -100,
    AV_NOT_INIT     = -99,
    AV_FILE_ERR     = -98,
    AV_STREAM_ERR   = -97,
    AV_MALLOC_ERR   = -96,
    AV_DECODE_ERR   = -95,
    AV_SEEK_ERR     = -94,
    AV_PARM_ERR     = -93,
    AV_NOT_FOUND    = -92,
    AV_SET_ERR      = -91,
    AV_CONFIG_ERR   = -90,
    AV_ENCODE_ERR   = -89,
    AV_TS_ERR       = -88,
    AV_FIFO_ERR     = -87,
    AV_NOT_SUPPORT  = -86,
    AV_NOT_ENOUGH   = -85,
    AV_TRANSFER_ERR = -84,
    AV_TIMEOUT      = -83,
    AV_EXIT_NORMAL       = 1,
} ErrorCode;
    
}
#endif /* MTError_h */
