//
//  VoiceTestApp.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/13.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include "ortp/ortp.h"
#include "g729a.h"

#include "VoiceTestApp.hpp"
#include "RtpConn.hpp"
#include "MediaHandleBase.hpp"
#include "MediaHandleFactory.hpp"
#include "FrameData.hpp"
#include "RtpPayload.h"
#include "RingBuffer.hpp"

#include "FFmpegUtils.h"

VoiceTestApp::VoiceTestApp()
{
    m_fileHandle = NULL;
    m_stat = false;
    m_rtpConn = NULL;
    m_time = 1;
}

VoiceTestApp::~VoiceTestApp()
{
    
}

int VoiceTestApp::setRemoteIp(const char *addr, int port)
{
    m_ipAddr = addr;
    m_rtpPort = port;
    
    return 0;
}

int VoiceTestApp::setLoop(int loopTimes)
{
    m_time = loopTimes;
    
    return 0;
}

int VoiceTestApp::setVoiceFile(const char *file)
{
    m_voiceFile = file;
    
    return 0;
}

int VoiceTestApp::setAudioPayload(int payload)
{
    m_payload = payload;
    
    return 0;
}

int VoiceTestApp::process()
{
    int ret;
    MMTOOLS::FrameData *frameData = NULL;
    uint8_t *audioData;
    size_t audioDataLen;
    
    if (m_rtpConn == NULL) {
        m_rtpConn = new RtpConn();
    }
    
    ret = m_rtpConn->create(m_ipAddr.c_str(), m_rtpPort, m_payload);
    if (ret < 0) {
        return ret;
    }
    
    if (m_fileHandle == NULL) {
        m_fileHandle = MediaHandleFactory::createMediaHandle();
        if (m_fileHandle == NULL) {
            av_log(NULL, AV_LOG_ERROR, "Coundn't create file handle\n");
            return AVERROR(ENOMEM);
        }
    }
    
    ret = m_fileHandle->open(m_voiceFile.c_str());
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Open file error!\n");
        return ret;
    }
    
    
    frameData = new MMTOOLS::FrameData();
    if (frameData == NULL) {
        av_log(NULL, AV_LOG_ERROR, "New frame data error!\n");
        return -1;
    }
    
    while (m_stat) {
        ret = m_fileHandle->read(frameData, 0);
        if (ret < 0) {
            if (ret == AVERROR_EOF || ret == AVERROR(EOF)) {
                if (m_time > 0) {
                    m_time--;
                    if (m_time <= 0) {
                        break;
                    }
                } else if (m_time == -1) {
                    m_fileHandle->reOpen(m_voiceFile.c_str());
                    continue;
                }
            }
            av_log(NULL, AV_LOG_ERROR, "Read audio data error!\n");
            break;
        }
        
        audioData = frameData->read(audioDataLen);
        if (audioData != NULL && audioDataLen > 0) {
            ring_put(m_ringbuffer, (unsigned char *)audioData, (unsigned int)audioDataLen);
        } else {
            av_log(NULL, AV_LOG_ERROR, "Audio data size or data error!\n");
            break;
        }
        
        while (ring_len(m_ringbuffer) >= 160) {
            ret = ring_get(m_ringbuffer, audioData, 160);
            if (ret > 0) {
                ret = m_rtpConn->sendData(audioData, 160);
                if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Send data error!\n");
                }
            } else {
                av_log(NULL, AV_LOG_ERROR, "Get data from ring error!\n");
                break;
            }
        }
    }
    
    if (frameData) {
        frameData->release();
        delete frameData;
    }
    
    return 0;
}

int VoiceTestApp::prepare()
{
    int ret = 0;
    
    if (m_rtpConn == NULL) {
        m_rtpConn = new RtpConn();
    }
    
    if (m_payload == G729_PAYLOAD) {
        va_g729a_init_encoder();
    }
    
    ret = m_rtpConn->create(m_ipAddr.c_str(), m_rtpPort, m_payload);
    if (ret < 0) {
        return ret;
    }
    
    if (m_fileHandle == NULL) {
        m_fileHandle = MediaHandleFactory::createMediaHandle();
        if (m_fileHandle == NULL) {
            av_log(NULL, AV_LOG_ERROR, "Coundn't create file handle\n");
            return AVERROR(ENOMEM);
        }
    } else {
        av_log(NULL, AV_LOG_WARNING, "FIle open twice!\n");
    }
    
    ret = m_fileHandle->open(m_voiceFile.c_str());
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Open file error!\n");
        return ret;
    }
    
    frameData = new MMTOOLS::FrameData();
    if (frameData == NULL) {
        av_log(NULL, AV_LOG_ERROR, "New frame data error!\n");
        return -1;
    }
    
    pthread_mutex_init(&mux, NULL);
    m_ringbuffer = ring_alloc(8*1024, &mux);
    if (m_ringbuffer == NULL) {
        return -1;
    }
    
    return ret;
}

int VoiceTestApp::readAudioData()
{
    int ret;
    uint8_t *audioData;
    size_t audioDataLen;
    
    ret = m_fileHandle->read(frameData, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "read error!\n");
        return ret;
    }
    
    audioData = frameData->read(audioDataLen);
    if (audioData != NULL && audioDataLen > 0) {
        ring_put(m_ringbuffer, (unsigned char *)audioData, (unsigned int)audioDataLen);
    } else {
        av_log(NULL, AV_LOG_ERROR, "Audio data size or data error!\n");
        return AVERROR(ENOMEM);
    }
    
    return 0;
}

int VoiceTestApp::sendAudioData()
{
    uint8_t pcmData[256];
    uint8_t *pSendData;
    int sendDataSize;
    int ret;
    
    if (ring_len(m_ringbuffer) >= 160) {
        ret = ring_get(m_ringbuffer, pcmData, 160);
        if (ret > 0) {
            short *s16PcmData = (short *)pcmData;
            uint8_t g729Data[32];
            if (m_payload == G729_PAYLOAD) {
                va_g729a_encoder(s16PcmData, g729Data);
                pSendData = g729Data;
                sendDataSize = L_FRAME_COMPRESSED;
                ret = m_rtpConn->sendData(pSendData, sendDataSize);
                if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Send data error!\n");
                }
            } else {
                ret = m_rtpConn->sendData(pcmData, 160);
                if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Send data error!\n");
                }
            }
        } else {
            av_log(NULL, AV_LOG_ERROR, "Get data from ring error![%d]\n", ret);
            return ret;
        }
    } else {
        return AVERROR(EAGAIN);
    }
    
    return 0;
}


bool VoiceTestApp::sessionSetIsSet()
{
    if (m_rtpConn == NULL) {
        return -1;
    }
    
    return m_rtpConn->isSet();
}

int VoiceTestApp::addToSet(SessionSet *set)
{
    if (m_rtpConn == NULL) {
        return -1;
    }
    
    return m_rtpConn->addToSet(set);
}

int VoiceTestApp::stop()
{
    return 0;
}


