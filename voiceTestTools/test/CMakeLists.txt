cmake_minimum_required (VERSION 2.6)

file(GLOB Test_Source 
    main.cpp
)

include_directories (
	${PROJECT_SOURCE_DIR}/src
	${OPEN_3RDPARTY_DIR}/src/base
	${OPEN_3RDPARTY_DIR}/src/g729
	${OPEN_3RDPARTY_DIR}/src/utils	
)

link_libraries (
	pthread
	dl
	m
  	offlineRtpSdk
)


add_executable(offlineRtpTools ${Test_Source})
