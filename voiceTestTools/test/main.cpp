//
//  main.cpp
//  voice_test_tools
//
//  Created by Adiya on 2018/11/13.
//  Copyright © 2018 xqm.co. All rights reserved.
//

#include <iostream>
#include <getopt.h>

#include "RtpConn.hpp"
#include "VoiceTestApp.hpp"
#include "MediaHandleFactory.hpp"

#include "RtpPayload.h"
#include "Gobal.hpp"
#include "RingBuffer.hpp"
#include "FFmpegUtils.h"
#include "MultiVoiceApp.hpp"

#define DEBUG_PRINT printf

#define DEBUG_OUTFILE_MODE 1


typedef struct PcapOption_t {
    std::string serverAdd;
    int serverMinPort;
    int serverMaxPort;
    int times;
    std::string dir;
    std::string logfile;
    int payload;
    int log;
    int limitStreams;
} PcapOption_t;

PcapOption_t gobalOptions;

enum {
    LONGOPT_VAL_TIMEOUT = 257, //注意我们定义的值从257开始，是为了避开短选项
    LONGOPT_VAL_MPORT
};

static const char *short_options = "c:vh";

static const struct option long_options[] = {
    {"server", required_argument, NULL, 's'},
    {"minPort", required_argument, NULL, 'm'},
    {"MaxPort", required_argument, NULL, 'M'},
    {"log", required_argument, NULL, 'l'},
    {"timeout", optional_argument, NULL, LONGOPT_VAL_TIMEOUT},
    {"times", required_argument, NULL, 't'},
    {"dir", required_argument, NULL, 'd'},
    {"payload", required_argument, NULL, 'p'},
    {"LimitStreams", required_argument, NULL, 'L'},
    {"help", no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
};

int getArguments(int argc, const char * argv[], PcapOption_t *pcapOption)
{
    float timeout = 0.0f;
    int verbose = 0;
    
    pcapOption->log = 0;
    pcapOption->times = 1;
    pcapOption->payload = G729_PAYLOAD;
    
    int opt = 0;
    while( (opt = getopt_long(argc, (char * const *)argv, (const char *)short_options, long_options, NULL)) != -1){
        switch (opt) {
            case 'h':
            case '?': //如果是不能识别的选项，则opt返回'?'
                fprintf(stdout, "Usage: %s -s <serverAddr> -m <min port> -M <max port> -d wavfiledir -p payload [-t times] [-h] [-f localfile] [-e networkcarkIndex] \n", argv[0]);
                return 0;
            case 's':
                pcapOption->serverAdd = optarg;
                break;
            case 'm':
                pcapOption->serverMinPort = atoi(optarg);
                break;
            case 'M': // 'p'是个短选项，但是长选项port的值也是'p'，所以这里同时处理了port
                pcapOption->serverMaxPort = atoi(optarg);
                break;
            case LONGOPT_VAL_TIMEOUT: //长选项timeout的值
                timeout = atof(optarg);
                break;
            case 'v': // verbose有可选参数，所以这儿判断是否有参数
                if(optarg == NULL){
                    verbose = 1;
                }
                else {
                    verbose = atoi(optarg);
                }
                break;
                
            case 'd':
                pcapOption->dir = optarg;
                
                break;
                
            case 'L':
                pcapOption->limitStreams = atoi(optarg);
                
                break;
                
            case 'l':
                pcapOption->log = atoi(optarg);
                
                break;
                
            case 'p':
                if (strcasecmp(optarg, "g729") == 0) {
                    pcapOption->payload = G729_PAYLOAD;
                } else if (strcasecmp(optarg, "ulaw") == 0) {
                    pcapOption->payload = PCMU_PAYLOAD;
                } else {
                    pcapOption->payload = G729_PAYLOAD;
                }
                
                break;
            default:
                printf("Not found this option: %c\n", opt);
                
                break;
        }
    }
    
    return 0;
}

int testMultiSendRtp(PcapOption_t *options)
{
    std::string filepath;
    int ret;
    int limit;
    int payload;
    int minPort;
    int maxPort;
    struct timeval begin, end;
    MultiVoiceApp *multiApp = new MultiVoiceApp();

    filepath = options->dir;
    limit = options->limitStreams;
    minPort = options->serverMinPort;
    maxPort = options->serverMaxPort;
    payload = options->payload;
    
    ret = multiApp->scandir(filepath, limit);
    if (ret < 0) {
        DEBUG_PRINT("Scan dir %s error!\n", filepath.c_str());
        return ret;
    }
    
    multiApp->setAuidoFormat(payload);
    multiApp->setRtpPortRange(minPort, maxPort);
    
    ret = multiApp->prepare();
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Prepare multi test error!\n");
        return ret;
    }
    
    gettimeofday(&begin, NULL);
    
    ret = multiApp->process();
    if ((ret != AVERROR_EOF) &&  ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Process error, break....[ret %d -- %d]\n", ret, AVERROR_EOF);
        return ret;
    }
    
    gettimeofday(&end, NULL);
    
    DEBUG_PRINT("Use time %ld\n", (end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec));
    
    multiApp->stop();
    
    delete multiApp;
    
    return 0;
}

int testSingleSendRtp()
{
    const char *dstip = "127.0.0.1";
    int dstPort = 8888;
    int ret;
    const char *pcmDataFile = "00637342.wav";
    struct timeval begin, end;
    RingBuffer *ringbuffer;
    
    unsigned char rtpData[256];
    MediaHandleBase *fileHandle;
    FILE *wavFileFp;
    MMTOOLS::FrameData *frameData = NULL;
    uint8_t *audioData;
    size_t dataSize;
    uint8_t *pData;
    pthread_mutex_t mux;
    
#if DEBUG_OUTFILE_MODE
    const char *pcmFilename = "/Users/xqm/Documents/testdir/audio/rawdata/s16le_8000.pcm";
    FILE *pcmFp;
    
    pcmFp = fopen(pcmFilename, "w+");
    if (pcmFp == NULL) {
        DEBUG_PRINT("Open file  %s error !\n", pcmFilename);
        return -1;
    }
#endif
    
    pthread_mutex_init(&mux, NULL);
    
    ringbuffer = ring_alloc(8*1024, &mux);
    if (ringbuffer == NULL) {
        return -1;
    }
    
    frameData = new MMTOOLS::FrameData();
    if (frameData == NULL) {
        DEBUG_PRINT("New frame data error!\n");
        return -1;
    }
    
    RtpConn *rtpconn = new RtpConn();
    if (rtpconn == NULL) {
        DEBUG_PRINT("New rtp connection error!\n");
        return -1;
    }
    
    ret = rtpconn->create(dstip, dstPort, 0);
    if (ret < 0) {
        DEBUG_PRINT("Create rtp connection error!\n");
        return ret;
    }
    
    wavFileFp = fopen(pcmDataFile, "rb");
    if (wavFileFp == NULL) {
        DEBUG_PRINT("Open file error %s !\n", pcmDataFile);
        return -1;
    }
    
    
    fileHandle = MediaHandleFactory::createMediaHandle();
    if (fileHandle == NULL) {
        DEBUG_PRINT("Coundn't create file handle\n");
        return -1;
    }
    
    ret = fileHandle->open(pcmDataFile);
    if (ret < 0) {
        DEBUG_PRINT("Open file %s error!\n", pcmDataFile);
        return ret;
    }
    
    gettimeofday(&begin, NULL);
    
    do {
        ret = fileHandle->read(frameData, 0);
        if (ret < 0) {
            DEBUG_PRINT("Read data error!\n");
            break;
        }
        audioData = frameData->read(dataSize);
        
#if DEBUG_OUTFILE_MODE
        if (dataSize > 0 && audioData != NULL) {
            ret = fwrite(audioData, 1, dataSize, pcmFp);
            if (ret <= 0) {
                DEBUG_PRINT("Write audio file error!\n");
            }
        }
#endif
        ring_put(ringbuffer, audioData, dataSize);
        
        while (ring_len(ringbuffer) >= 160) {
            
            ret = ring_get(ringbuffer, rtpData, 160);
            if (ret > 0) {
                ret = rtpconn->sendData(rtpData, 160);
                if (ret < 0) {
                    DEBUG_PRINT("Send data error!\n");
                }
            }
        }
        
    } while (1);
    gettimeofday(&end, NULL);
    
    DEBUG_PRINT("Use time %ld", (end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec));
    
    if (fileHandle) {
        fileHandle->close();
        delete fileHandle;
    }
    
    if (rtpconn) {
        rtpconn->release();
        delete rtpconn;
    }
    
    return 0;
}

int main(int argc, const char * argv[])
{
    int ret;
    std::string path;

    ret = getArguments(argc, argv, &gobalOptions);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Please use valid parameter\n");
        return ret;
    }
    
    Gobal::init();
    DEBUG_PRINT("Send data begin...\n");
    testMultiSendRtp(&gobalOptions);
    Gobal::deinit();

    DEBUG_PRINT("Send data end...\n");
    

    
    return ret;
}
